//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
#pragma once

#include <tchar.h>
#include <xstring>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

////////////////////////////////////
//Allready defined in tchar.h
//number -> text
//--------------
//_tprintf
//_tfprintf
//_stprintf_s
//_itot_s
//_i64tot_s
//_ui64tot_s

//text -> number
//--------------
//_tcstod	double
//_tstoi	integer
////////////////////////////////////

#ifdef _UNICODE
//strings
#define tstring wstring

//streams
#define tcout wcout
#define tofstream wofstream
#define tifstream wifstream
#define tfstream wfstream
#define tstringstream wstringstream


#else
//strings
#define tstring string

//streams
#define tcout cout
#define tofstream ofstream
#define tifstream ifstream
#define tstringstream stringstream

#endif

/*
size_t tstringsize(const wstring& tstring);
size_t tstringsize(const string& tstring);
*/


//!Returns the number of bytes for string content.
static inline size_t tstringsize(const tstring& tstring)
{
	return tstring.length()*sizeof(_TCHAR);
}

static inline tstring toTString(string &str)
{
	return tstring(str.begin(),str.end());
}

static inline tstring toTString(wstring &str)
{
	return tstring(str.begin(),str.end());
}

#include<sstream>
static inline tstring toTString(const char* str)
{
	string sstr(str);
	return tstring(sstr.begin(),sstr.end());
}

static inline tstring toTString(const wchar_t* str)
{
	wstring sstr(str);
	return tstring(sstr.begin(),sstr.end());
}
