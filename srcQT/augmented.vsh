#version 150
in vec3 position;
in vec3 inColor;
in vec2 texCoord;

uniform mat4 MVP;

out vec3 ourColor;
out vec2 TexCoord;

void main()
{
    ourColor = inColor;
    TexCoord = texCoord;
	gl_Position = MVP * vec4(position, 1.0f);
}