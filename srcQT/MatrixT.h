//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
//! General Matrix Template.
#pragma once

#include <iostream>
using namespace std;

#include <math.h>
#include "VectorT.h"
//#include "DynMatrixT.h"

#include <memory.h>
#include <stdarg.h>
//#include "Polynomial.h"
//#include <DbgMemoryLeaks.h>
//#include <cmath>


//template <class T,size_t _r, size_t _c>
//class MatrixT;
//template <class T>
//class VectorT;
/*
typedef MatrixT<char> Matrixc;
typedef MatrixT<unsigned char> Matrixuc;
typedef MatrixT<short> Matrixs;
typedef MatrixT<unsigned short> Matrixus;
typedef MatrixT<int> Matrixi;
typedef MatrixT<unsigned int> Matrixui;
typedef MatrixT<float> Matrixf;
typedef MatrixT<double> Matrixd;
*/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!@brief MatrixT<T>template class
//!
//! This class represents a nxm Matrix.
template<class T,size_t _r, size_t _c>
class MatrixT
{
	friend class VectorT<T,_r>;  // Declare a friend class
	//friend class MatrixT<T,_r-1,_c-1>;

public:
	//! Matrix data
	T* m_values;

	//! Number of rows
//	size_t m_rows;
	//! Number of columns
//	size_t m_columns;

	bool m_bRefData;	//!< true if m_values was not created by MatrixT

public:
	
	//! Creates an empty mxn matrix
	//!\param m The number of rows
	//!\param n The number of columns
	MatrixT()
	{
		m_bRefData=false;
		m_values=new T[_r*_c];
		memset(m_values,0,_r*_c*sizeof(T));
		identity();
	}

	//! Copy constructor
	//! is called when MatrixT<type> mtx2=mtx1;
	MatrixT(const MatrixT& mtx)
	{
		m_bRefData=false;
		m_values=new T[_r*_c];
		memcpy(m_values,mtx.m_values,_r*_c*sizeof(T));
	}

	//! Creates a mxn matrix and initializes with values from arr
	//!\param m The number of rows
	//!\param n The number of columns
	//!\param arr Array of size m*n. Note: Consider the first m elements in arr as
	//! the elements for the first column vector of the matrix.
	MatrixT(T* arr, bool bRefData=false)
	{
		m_bRefData=bRefData;
		if(m_bRefData)
		{
			m_values=arr;
		}
		else
		{
			m_values=new T[_r*_c];
			memcpy(m_values,arr,_r*_c*sizeof(T));
		}
	}

	//! Creates a matrix from n column vectors. Note all vectors must have the same size m.
	//!\param n number of columns
	//!\param ... argument list contains the column vectors VectorT<T>.
	//! Be sure that vector elements are T type.
	MatrixT(VectorT<T,_r> v0, ...)
	{
		m_bRefData=false;
		m_values=new T[_r*_c];
		memset(m_values,0,_r*_c*sizeof(T));

		VectorT<T,_r> *v;
		T* pValues=m_values;
		memcpy(pValues,v0.getValues(),_r*sizeof(T));
		pValues+=_r;

		va_list vl;
		va_start(vl,v0);
		//Get the first vector to get the number of rows

		for(size_t i=1;i<_c;i++,pValues+=_r)
		{
			v=(VectorT<T,_r>*)&va_arg(vl,VectorT<T>);
			memcpy(pValues,v->getValues(),_r*sizeof(T));
		}
		
		va_end(vl);
	}

	//! Destructor
	~MatrixT(void)
	{
		if(!m_bRefData)
			delete[] m_values;
	}

	//! //Subscript operator
	//! Returns the address of the first element of column j.
	//! This operator can be used to access matrix elements directly.
	//! B[1][2] means get access to the element in third row and second column of Matrix B.
	T* operator[](size_t j)
	{
		return &m_values[j*_r];
	}

	//! access grants 
	T& operator () ( size_t row, size_t col)
	{
		//((float*)_m)[4*row + col];//cols*row+col
		return m_values[row+col*_r];
	}
	//! access grants
	T  operator () ( size_t row, size_t col ) const
	{
		return m_values[row+col*_r];
	}

	//!\return The value array
	T* getValues()
	{
		return m_values;
	}

	//! Sets matrix data
	//!\param values Must have the same number of elements like m_values
	void set(T* values)
	{
		memcpy(m_values,values,getSize()*sizeof(T));
	}

	//!\return The number of rows
	size_t getRows(){return _r;}

	//!\return The number of columns
	size_t getColumns(){return _c;}

	//!\return The number of elements in matrix.
	size_t getSize(){return _r*_c;}
	

	//! Sets column vector j.
	//!\param j the column index
	//!\param arr an array containing column elements which size must be equal to m_rows
	void setColumn(size_t j, T* arr)
	{
		memcpy(&m_values[j*_r],arr,sizeof(T)*_r);
	}


	//! Sets column vector j.
	//!\param j the column index
	//!\param v the new column vector
	void setColumn(size_t j, VectorT<T,_r>& v)
	{
		T *p=&m_values[j*_r];
		memcpy(p,v.getValues(),sizeof(T)*_r);
	}

	//! Getting column vector j
	VectorT<T,_r> getColumnVect(size_t j)
	{
		T *p=&m_values[j*_r];
		VectorT<T,_r> v;
		memcpy(v.getValues(),p,sizeof(T)*_r);
		return v;
	}

	//! Sets row vector i.
	//!\param i the row index
	//!\param v the new row vector
	void setRow(size_t i, VectorT<T,_c>& v)
	{
		T* vp=v.getValues();
		T* mp=&m_values[i];
		T* pvEnd=&v.getValues()[_c];
		for(;vp<pvEnd;vp++,mp+=_r)
		{
			*mp=*vp;
		}
	}

	//! Sets row vector i.
	//!\param i the row index
	//!\param arr Array with elements for the new row vector
	void setRow(size_t i, T* arr)
	{
		T* p;
		T* pEnd=&m_values[_r*_c];
		T* t;
		for (p=&m_values[i],t=arr;p<pEnd;p+=_r,t++)
		{
			*p=*t;
		}
	}

	//! Getting row vector i
	VectorT<T,_c> getRowVect(size_t i)
	{
		VectorT<T,_c> v;
		T* vp;
		T* mp;
		T* pmpEnd=&m_values[getSize()];
		for (mp=&m_values[i],vp=v.getValues();mp<pmpEnd;mp+=_r,vp++)
		{
			*vp=*mp;
		}
		return v;
	}

	//!\return minimum element
	T minElement()
	{
		T minEl=m_values[0];
		T* pEnd=&m_values[getSize()];
		for(T* p = &m_values[1];p<pEnd;p++)
		{
			if(*p<minEl)
				minEl=*p;
		}
		return minEl;
	}

	//!\return maximum element
	T maxElement()
	{
		T maxEl=m_values[0];
		T* pEnd=&m_values[getSize()];
		for(T* p = &m_values[1];p<pEnd;p++)
		{
			if(*p>maxEl)
				maxEl=*p;
		}
		return maxEl;
	}

	//!Sum of all elements
	T sum()
	{
		T s=0;
		T* pEnd=&m_values[getSize()];
		for(T* p = &m_values[0];p<pEnd;p++)
		{
			s+=*p;
		}
		return s;
	}

	//!\return Matrtrix 
	MatrixT<T,_r,_c> round()
	{
		MatrixT<T,_r,_c> mtxRes;
		T* q=mtxRes.getValues();
		
		T* pEnd=&m_values[getSize()];
		for (T* p=m_values;p<pEnd;p++,q++)
		{
			if(*p>0)
				*q=int(*p+0.5);
			else
				*q=int(*p-0.5);
		}
		
		return mtxRes;
	}

	//! The maximum elements in matrix will be v.
	MatrixT<T,_r,_c>& scaleTo(const T& v)
	{
		T scaleFactor=v/maxElement();
		*this*=scaleFactor;
		return *this;
	}

	//! Normalize Matrix
	MatrixT<T,_r,_c>& normalize()
	{
		T elSum=sum();
		*this/=elSum;
		return *this;
	}

	//! Devide by a constant.
	MatrixT<T,_r,_c>& operator/=(const T& e)
	{
		T* pEnd=&m_values[getSize()];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p/=e;
		}
		return *this;
	}


	//! Add a constant value to all elements.
	MatrixT<T,_r,_c>& operator+=(const T& e)
	{
		T* pEnd=&m_values[getSize()];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p+=e;
		}
		return *this;
	}

	//! Add a subtract constant value from all elements.
	MatrixT<T,_r,_c>& operator-=(const T& e)
	{
		T* pEnd=&m_values[getSize()];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p-=e;
		}
		return *this;
	}

	//! Multiply by a constant.
	MatrixT<T,_r,_c>& operator*=(const T& e)
	{
		T* pEnd=&m_values[getSize()];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p*=e;
		}
		return *this;
	}

	

	//! Add a constant value to all elements.
	MatrixT<T,_r,_c> operator+(const T& e)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		for (p=m_values, q=t.m_values;p<pEnd;p++,q++)
		{
			*q=*p+e;
		}
		return t;
	}

	//! Subtract a constant value from all elements.
	MatrixT<T,_r,_c> operator-(const T& e)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		for (p=m_values, q=t.m_values;p<pEnd;p++,q++)
		{
			*q=*p-e;
		}
		return t;
	}

	//! Multiply by a constant.
	MatrixT<T,_r,_c> operator*(const T& e)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		for (p=m_values, q=t.m_values;p<pEnd;p++,q++)
		{
			*q=*p*e;
		}
		return t;
	}

	//! Divide by a constant.
	MatrixT<T,_r,_c> operator/(const T& e)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* q;
		T* pEnd=&m_values[getSize()];
		for (p=m_values, q=t.m_values;p<pEnd;p++,q++)
		{
			*q=*p/e;
		}
		return t;
	}

	//! Fills the matrix with a constant value.
	MatrixT<T,_r,_c>& fill(const T& e)
	{
		T* pEnd=&m_values[getSize()];
		for(T* p = m_values;p<pEnd;p++)
		{
			*p=e;
		}
		return *this;
	}

	//! Make identity Matrix.
	MatrixT<T,_r,_c>& identity()
	{
		memset(m_values,0,getSize()*sizeof(T));
		T* p;
		T* pEnd=&m_values[getSize()];
		for(p=m_values;p<pEnd;p+=_r+1)
		{
			*p=1;
		}
		return *this;
	}

	//! Make zero Matrix
	MatrixT<T,_r,_c>& zero()
	{
		memset(m_values,0,getSize()*sizeof(T));
		return *this;
	}

	//! Exchanges two column vectors in the matrix
	void exchangeColumns(size_t a,size_t b)
	{
		if(a!=b)
		{
			T* t=new T[_r];
			memcpy(t,&m_values[a*_r],_r*sizeof(T));
			memcpy(&m_values[a*_r],&m_values[b*_r],_r*sizeof(T));
			memcpy(&m_values[b*_r],t,_r*sizeof(T));
			delete[] t;
		}
	}

	//! Exchanges two row vectors in the matrix.
	void exchangeRows(size_t a,size_t b)
	{
		if (a!=b)
		{
			T* ra;
			T* praEnd=&m_values[getSize()];
			T* rb;
			T t;
			for(ra=&m_values[a],rb=&m_values[b];ra<praEnd;ra+=_r,rb+=_r)
			{
				t=*ra;
				*ra=*rb;
				*rb=t;
			}
		}
	}

	MatrixT<T,_c,_r> getTranspose()
	{
		T* p;
		size_t r=0;
		MatrixT<T,_c,_r> mtx;
		T* pEnd=&m_values[getSize()];
		for(p=m_values;p<pEnd;p+=_r)
		{
			mtx.setRow(r++,p);
		}
		return mtx;
	}


	//! Transposes nxn matrix elements
	//! Number of rows and columns are exchanged.
	MatrixT<T,_r,_r>& transpose()
	{
		T* p;
		size_t r=0;
		MatrixT<T,_r,_r> mtx;
		T* pEnd=&m_values[getSize()];
		for(p=m_values;p<pEnd;p+=_r)
		{
			mtx.setRow(r++,p);
		}
		set(mtx.m_values);
		return *this;
	}


	//! The minor is the resulting matrix if row i and column j are removed.
	//!\return A minor matrix
	MatrixT<T,_r-1,_c-1> getMinor(size_t nI, size_t nJ)
	{
		MatrixT<T,_r-1,_c-1> minor;
		T* t=minor.m_values;
		for (size_t j=0;j<_c;j++)
			for(size_t i=0;i<_r;i++)
			{
				if(i!=nI && j!=nJ)
				{
					*(t++)=(*this)[j][i];//Element from row i column j
					//int a=0;
				}
			}
			return minor;
	}


	//! Laplacian computation of the determinant
	//!\return The determinant of nxn matrix
	T det()
	{
		return DynMatrixT<T>(_r,_c,m_values,true).det();
	}

	//!\return The inverse.
	//! Note: If matrix is orthogonal then the inverse is equal to the transpose.
	MatrixT<T,_r,_r> getInverse()
	{
		MatrixT<T,_r,_r> mtx;
		for ( size_t i = 0; i < _c; ++i)
			for ( size_t j = 0; j < _c; ++j)
			{
				int sgn = ( (i+j)%2) ? -1 : 1;
				mtx[j][i] = getMinor(i,j).det() * sgn;
			}
			mtx.transpose();
			mtx=mtx/det();
			return mtx;
	}

	//! Invert a nxn matrix
	MatrixT<T,_r,_r>& invert()
	{
		MatrixT<T,_r,_r> m=getInverse();
		set(m.m_values);
		return *this;
	}

	//! The copy operator
	MatrixT &operator=(const MatrixT<T,_r,_c> &mtxB)
	{
		delete[] m_values;
		m_values=new T[_r*_c];
		memcpy(m_values,mtxB.m_values,_r*_c*sizeof(T));
		return *this;
	}

	/*
	//! Computing eigen values
	DynMatrixT<T> getEigenValues()
	{
		DynMatrixT<T> s(_r,_c,m_values,true);
		return s.getEigenValues();
	}
	*/

	/*
	//!\return characteristic polynomial
	Polynomial getCharacteristicPolynomial()
	{
		DynMatrixT<T> s(_r,_c,m_values,true);
		return s.getCharacteristicPolynomial();
	}
	*/
	

	//! absolute values
	MatrixT<T,_r,_c>& abs()
	{
		T* pEnd=&m_values[getSize()];
		for(T* p=m_values;p<pEnd;p++)
		{
			*p=T(std::abs((double)*p));
		}
		return *this;
	}

	//! Power all elements by e
	template<class U>
	MatrixT<T,_r,_c>& pow(U e)
	{
		T* pEnd=&m_values[getSize()];
		for(T* p=m_values;p<pEnd;p++)
		{
			*p=std::pow(*p,e);
		}
		return *this;
	}

	//! Natural exponential of the elements
	MatrixT<T,_r,_c>& exp()
	{
		T* pEnd=&m_values[getSize()];
		for(T* p=m_values;p<pEnd;p++)
		{
			*p=T(std::exp((double)*p));
		}
		return *this;
	}

	//! Logarithm naturalis
	MatrixT<T,_r,_c>& ln()
	{
		T* pEnd=&m_values[getSize()];
		for(T* p=m_values;p<pEnd;p++)
		{
			*p=T(std::log((double)*p));
		}
		return *this;
	}

	//! Base 10 logarithm
	MatrixT<T,_r,_c>& lg()
	{
		T* pEnd=&m_values[getSize()];
		for(T* p=m_values;p<pEnd;p++)
		{
			*p=T(std::log10((double)*p));
		}
		return *this;
	}


	//! Multiply column j by a constant value
	void mulColumn(size_t j, const T& val)
	{
		T *p=&m_values[j*_r];
		for(size_t i=0;i<_r;i++)
			p[i]=p[i]*val;
	}

	//! Multiply row i by a constant value
	void mulRow(size_t i, const T& val)
	{
		T* p;
		T* pEnd=&m_values[getSize()];
		for (p=&m_values[i];p<pEnd;p+=_r)
		{
			*p=*p*val;
		}
	}



	//! Adds column i to column j
	void addColumnToColumn(size_t i, size_t j)
	{
		T *pi=&m_values[i*_r];
		T *pj=&m_values[j*_r];
		for(size_t t=0;t<_r;t++)
			pj[t]=pj[t]+pi[t];
	}

	//! Adds row i to row j
	void addRowToRow(size_t i, size_t j)
	{
		T* pi;
		T* piEnd=&m_values[getSize()];
		T* pj;
		for (pi=&m_values[i],pj=&m_values[j];pi<piEnd;pi+=_r,pj+=_r)
		{
			*pj=*pj+*pi;
		}
	}

	//! Subtracts column i from column j
	void subColumnFromColumn(size_t i, size_t j)
	{
		T *pi=&m_values[i*_r];
		T *pj=&m_values[j*_r];
		for(size_t t=0;t<_r;t++)
			pj[t]=pj[t]-pi[t];
	}

	//! Subtracts row i from row j
	void subRowFromRow(size_t i, size_t j)
	{
		T* pi;
		T* piEnd=&m_values[getSize()];
		T* pj;
		for (pi=&m_values[i],pj=&m_values[j];pi<piEnd;pi+=_r,pj+=_r)
		{
			*pj=*pj-*pi;
		}
	}


	//! Addition of matrices.
	MatrixT<T,_r,_c>& operator+=(const MatrixT<T,_r,_c>& mtx)
	{
		T* p=m_values;
		T* q=mtx.m_values;
		T* pEnd=&m_values[getSize()];
		for (;p<pEnd;p++,q++)
		{
			*p += *q;
		}
		return *this;
	}

	//! Subtraction of matrices.
	MatrixT<T,_r,_c>& operator-=(const MatrixT<T,_r,_c>& mtx)
	{
		T* p=m_values;
		T* q=mtx.m_values;
		T* pEnd=&m_values[getSize()];
		for (;p<pEnd;p++,q++)
		{
			*p -= *q;
		}
		return *this;
	}

	//! Addition of matrices.
	MatrixT<T,_r,_c> operator+(const MatrixT<T,_r,_c>& mtx)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		T* r;
		for (p=m_values, q=mtx.m_values, r=t.m_values;p<pEnd;p++,q++,r++)
		{
			*r=*p+*q;
		}
		return t;
	}

	//! Subtraction of matrices.
	MatrixT<T,_r,_c> operator-(const MatrixT<T,_r,_c>& mtx)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		T* r;
		for (p=m_values, q=mtx.m_values, r=t.m_values;p<pEnd;p++,q++,r++)
		{
			*r=*p-*q;
		}
		return t;
	}

	//! Multiply corresponding elements
	MatrixT<T,_r,_c> mulElementByElement(const MatrixT<T,_r,_c>& mtx)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		T* r;
		for (p=m_values, q=mtx.m_values, r=t.m_values;p<pEnd;p++,q++,r++)
		{
			*r=*p * *q;
		}
		return t;
	}

	//! Devide corresponding elements
	MatrixT<T,_r,_c> divElementByElement(const MatrixT<T,_r,_c>& mtx)
	{
		MatrixT<T,_r,_c> t;
		T* p;
		T* pEnd=&m_values[getSize()];
		T* q;
		T* r;
		for (p=m_values, q=mtx.m_values, r=t.m_values;p<pEnd;p++,q++,r++)
		{
			*r=*p / *q;
		}
		return t;
	}

	//! Matrix multiplication
	template<size_t _cb>
	MatrixT<T,_r,_cb> operator*(MatrixT<T,_c,_cb>& mtxB)
	{
		MatrixT<T,_r,_cb> t;
		//t.fill(0);
		T* a;
		T* b;
		T* c;

		a=m_values;
		size_t na=_r*_c;
		b=mtxB.m_values;
		size_t nb=_c*_cb;
		c=t.m_values;
		//*c=0;
		for (b=mtxB.m_values;b<&mtxB.m_values[nb];b+=_c)
		{
			a=m_values;
			for(a=m_values;a<&m_values[_r];a++)
			{
				*c = 0;
				for (;a<&m_values[na];a+=_r,b++)
				{
					*c+=*a * *b;
				}
				a-=na;
				b-=_c;
				c++;
			}
		}
		return t;
	}

	//!\return imaginary parts of elements as new matrix.
	MatrixT<double,_r,_c> imag()
	{
		MatrixT<double,_r,_c> mtxRes;
		__if_exists(T::complex)//Test if complex data type
		{
			T* p=m_values;
			double* pRes=mtxRes.getValues();
			double* pEnd=mtxRes.getValues()+mtxRes.getSize();
			for(;pRes<pEnd;pRes++,p++)
			{
				*pRes=p->imag();
			}
			return mtxRes;
		}
		return mtxRes;
	}

	//!\return real parts of elements as new matrix.
	MatrixT<double,_r,_c> real()
	{
		__if_exists(T::complex)//Test if complex data type
		{
			MatrixT<double,_r,_c> mtxRes(m_rows,m_columns);
			T* p=m_values;
			double* pRes=mtxRes.getValues();
			double* pEnd=mtxRes.getValues()+mtxRes.getSize();
			for(;pRes<pEnd;pRes++,p++)
			{
				*pRes=p->real();
			}
			return mtxRes;
		}
		__if_not_exists(T::complex)
		{
			return *this;
		}
	}
	

	//! Matrix Vector Multiplication
	VectorT<T,_r> operator*(VectorT<T,_c>& vec);

	

	//! type cast operator
	template<class U>
	operator MatrixT<U,_r,_c>();

	//! Standard output
	template<class T>
	friend std::ostream& operator<<(std::ostream &s, MatrixT<T,_r,_c> &mtx);
	
};
//!stdout operator
template<class T,size_t _r,size_t _c>
std::ostream& operator<<(std::ostream& s, MatrixT<T,_r,_c>& mtx)
{
	T *p=0;
	s << "Matrix:\n";
	for (size_t j=0; j<_r; j++)
	{
		p=&mtx.getValues()[j];
		for (size_t i=0; i<_c; i++)
		{
			s << *p <<" ";
			p+=_r;
		}
		s << "\n";
	}
	return s;
}


//!Type Cast operator
template<class T,size_t _r,size_t _c> template<class U>
MatrixT<T,_r,_c>::operator MatrixT<U,_r,_c>()
{
	size_t arrSize=getSize();
	U* arr=new U[arrSize];
	U* p;
	T* q;
	for (p=arr,q=m_values;p<&arr[arrSize];p++,q++)
	{
		*p=T(*q);
	}
	MatrixT<U,_r,_c> mtx(_r,_c,arr);
	delete[] arr;
	return mtx;
}



//!Matrix Vector Multiplication
template<class T,size_t _r, size_t _c>
VectorT<T,_r> MatrixT<T,_r,_c>::operator *(VectorT<T,_c> &vec)
{
	T* vecValues=vec.getValues();
	VectorT<T,_r> resVec;
	T* resValues=resVec.getValues();
	T* m=m_values;
	for(T* vVec=vecValues;vVec<&vecValues[_c];vVec++)
	{
		for (T* vRes=resValues;vRes<&resValues[_r];vRes++,m++)
		{
			*vRes+=*vVec* *m;
		}
	}
	return resVec;
}
