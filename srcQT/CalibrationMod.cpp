#include "CalibrationMod.h"

CalibrationMod::CalibrationMod(void)
{
	//setParametersToDefault();
}

CalibrationMod::~CalibrationMod(void)
{
}

void CalibrationMod::setName(std::tstring strName)
{
	m_strName = strName;
}

ControlPointT<double, 2> CalibrationMod::model(ControlPointT<double, 3>& pass3D)
{
	//VectorT<double, 3> v3(&pass3D._x);
	ControlPointT<double, 2> point;
	point.coord() = model(pass3D.coord());
	point.id() = pass3D.id();
	return point;
}

list<ControlPointT<double, 2>>* CalibrationMod::computeResiduals()
{//m_lstPass2D and m_lstPass3D have to be sorted
	m_lstResiduals.clear();
	list<ControlPointT<double, 2>>::iterator itPassInImage = m_lstPass2D.begin();
	list<ControlPointT<double, 3>>::iterator itPass3D = m_lstPass3D.begin();
	for (; itPassInImage != m_lstPass2D.end(); itPassInImage++)
	{
		while (itPass3D != m_lstPass3D.end() && itPass3D->id() != itPassInImage->id())
		{
			itPass3D++;
		}
		VectorT<double, 2> projection = model(itPass3D->coord());
		ControlPointT<double, 2> cpRes(itPassInImage->coord() - projection,
									itPass3D->id());
		m_lstResiduals.push_back(cpRes);
	}
	return &m_lstResiduals;
}

VectorT<double, 2> CalibrationMod::meanAbsResidualsXY()
{
	VectorT<double, 2> absResXY;
	list<ControlPointT<double, 2>>::iterator itRes = m_lstResiduals.begin();
	for (; itRes != m_lstResiduals.end(); itRes++)
	{
		absResXY+=VectorT<double,2>(abs(itRes->coord()[0]), abs(itRes->coord()[1]));
	}
	absResXY /= (double)m_lstResiduals.size();
	return absResXY;
}

void CalibrationMod::setPass2DList(const list<ControlPointT<double, 2>> &pass2DList)
{
	m_lstPass2D = pass2DList;
}

size_t CalibrationMod::loadPass2DList(const TCHAR* fileName/*, list<PassPoint2D>& lstPass*/)
{
	return loadPass2DList(fileName, &m_lstPass2D);
}

size_t CalibrationMod::loadPass2DList(const TCHAR* fileName, list<ControlPointT<double, 2>>* pPass2DList)
{
	pPass2DList->clear();
	TCHAR *ext = (TCHAR*)&fileName[_tcslen(fileName) - 4];
	tifstream infile(fileName);
	if (infile.is_open())
	{
		ControlPointT<double, 2> tmp;

		if (_tcsicmp(ext, _T(".dat")) == 0)
		{//Winkalib dat file
			TCHAR buffer[500] = { 0 };
			double tDouble;
			TCHAR tChar;
			bool bWinkalibFile = false;
			infile.getline(buffer, sizeof(buffer));//Skip two comment lines
			infile.getline(buffer, sizeof(buffer));
			if (buffer[0] == _T('/'))
			{
				bWinkalibFile = true;
			}

			if (bWinkalibFile)
			{
				while (!infile.eof())
				{
					infile >> tmp.id() >> tmp.coord()[0] >> tmp.coord()[1] >> tDouble >> tDouble >> tChar;
					pPass2DList->push_back(tmp);
				}
				pPass2DList->pop_back();
			}
			else
			{
				infile.seekg(0, ios_base::beg);
				while (!infile.eof())
				{
					infile >> tmp.id() >> tmp.coord()[0] >> tmp.coord()[1] >> tDouble >> tDouble;
					pPass2DList->push_back(tmp);
				}
				pPass2DList->pop_back();
			}
		}
		else
		{//Simple text file is assumed. Each line looks in this way: ID X Y
			while (!infile.eof())
			{
				infile >> tmp.id() >> tmp.coord()[0] >> tmp.coord()[1];
				if (infile.eof())
					break;
				pPass2DList->push_back(tmp);
			}
		}

		infile.close();
	}
	else
	{
		tcout << _T("Could not load file: ") << fileName << endl;
	}
	pPass2DList->sort();
	return pPass2DList->size();
}


int CalibrationMod::savePass2DList(TCHAR* fileName, list<ControlPointT<double, 2>>* pPass2DList/*=NULL*/)
{
	if (pPass2DList == NULL)
		pPass2DList = &m_lstPass2D;

	tofstream outfile(fileName);
	outfile.precision(10);
	//outfile.flags();
	if (outfile.is_open())
	{
		//outfile << fixed << "[EXTERN]" << endl
		//typedef  iter;
		for (list<ControlPointT<double, 2>>::iterator p = pPass2DList->begin(); p != pPass2DList->end(); p++)
		{
			outfile << fixed << (*p).id() << _T('\t') << (*p).coord()[0] << _T('\t') << (*p).coord()[1] << _T('\t') << 0.0 << _T('\t') << 0.0 << endl;
		}

		outfile.close();
		return 1;
	}
	return 0;
}

size_t CalibrationMod::loadPass3DList(const TCHAR* fileName)
{
	m_lstPass3D.clear();
	ifstream infile(fileName);
	TCHAR *ext = (TCHAR*)&fileName[_tcslen(fileName) - 4];
	if (infile.is_open())
	{
		ControlPointT<double, 3> tmp;

		while (!infile.eof())
		{
			infile >> tmp.id() >> tmp.coord()[0] >> tmp.coord()[1] >> tmp.coord()[2];
			if (infile.eof())
				break;
			m_lstPass3D.push_back(tmp);
		}
		infile.close();

		if (_tcsicmp(ext, _T(".cor")) == 0)
		{//Winkalib/EllipseFinder cor file (pass.cor) .. remove invalide start and end entry
			//int t = 0;
			if (m_lstPass3D.back().id()<0)
				m_lstPass3D.pop_back();
			if (m_lstPass3D.front().id()<0)
				m_lstPass3D.pop_front();
		}
	}
	m_lstPass3D.sort();
	return m_lstPass3D.size();
}

size_t CalibrationMod::loadPass3DList(const TCHAR* fileName, list<ControlPointT<double, 3>>* pPass3DList)
{
	pPass3DList->clear();
	ifstream infile(fileName);
	TCHAR *ext = (TCHAR*)&fileName[_tcslen(fileName) - 4];
	if (infile.is_open())
	{
		ControlPointT<double, 3> tmp;


		while (!infile.eof())
		{
			infile >> tmp.id() >> tmp.coord()[0] >> tmp.coord()[1] >> tmp.coord()[2];
			if (!infile.eof())
				pPass3DList->push_back(tmp);
		}
		infile.close();

		if (_tcsicmp(ext, _T(".cor")) == 0)
		{//Winkalib/EllipseFinder cor file (pass.cor) .. remove invalide start and end entry
			int t = 0;
			pPass3DList->pop_back();
			pPass3DList->pop_front();
		}
	}
	return pPass3DList->size();
}

int CalibrationMod::savePass3DList(const TCHAR* fileName, list<ControlPointT<double, 3>>* pPass3DList)
{
	tofstream outfile(fileName);
	outfile.precision(10);
	//outfile.flags();
	if (outfile.is_open())
	{
		//outfile << fixed << "[EXTERN]" << endl
		//typedef  iter;
		for (list<ControlPointT<double, 3>>::iterator p = pPass3DList->begin(); p != pPass3DList->end(); p++)
		{
			outfile << fixed << (*p).id() << _T('\t') << (*p).coord()[0] << _T('\t') << (*p).coord()[1] << _T('\t') << (*p).coord()[2] << endl;
		}

		outfile.close();
		return 1;
	}
	return 0;
}

list<ControlPointT<double, 2>>* CalibrationMod::getPass2DList()
{
	return &m_lstPass2D;
}

list<ControlPointT<double, 3>>* CalibrationMod::getPass3DList()
{
	return &m_lstPass3D;
}