#include "cvToGL.h"

using namespace cv;

cvToGL::cvToGL()
{
	glfwInit();
	window = glfwCreateWindow(800, 600, "OpenGL", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (err != GLEW_OK)
		cout << "glewInit failed, aborting. Code " << err << ". " << endl;
	glViewport(0, 0, 800, 600);

	//image = SOIL_load_image("C:\\Users\\Datta\\Desktop\\woodenBox.jpg", &texWidth, &texHeight, 0, SOIL_LOAD_RGB);
	//cout << "SOIL read" << endl;
}

//cvToGL::cvToGL(Mat im)
//{
//	cout << "issues with constructor with opencv Mat " << endl;
//	/*glfwInit();
//	window = glfwCreateWindow(im.cols, im.rows, "OpenGL", nullptr, nullptr);
//	glfwMakeContextCurrent(window);
//	glewExperimental = GL_TRUE;
//	GLenum err = glewInit();
//
//	if (err != GLEW_OK)
//		cout << "glewInit failed, aborting. Code " << err << ". " << endl;
//	glViewport(0, 0, im.cols, im.rows);*/
//	
//}

GLuint cvToGL::compileShaders(GLuint mode, string& str)
{
	const GLchar* source = str.c_str();
	GLuint shader = glCreateShader(mode);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	char error[1000];
	glGetShaderInfoLog(shader, 1000, NULL, error);
	cout << "Shader " << mode << " \n Compile message: " << error << endl;
	return shader;
}

void cvToGL::loadShaders(const char* fn, string& str)
{
	ifstream inFile(fn);
	if (!inFile.is_open())
	{
		cout << "File " << fn << " could not be opened !!! inFile.is_open() is false" << endl;
		return;
	}
	char tmp[300];
	while (!inFile.eof())
	{
		inFile.getline(tmp, 300);
		str = str + tmp;
		str = str + '\n';
	}
}

void cvToGL::initShaders(const char* vshader, const char* fshader)
{
	string source;
	loadShaders(vshader, source);
	vs = compileShaders(GL_VERTEX_SHADER, source);
	source = "";
	loadShaders(fshader, source);
	fs = compileShaders(GL_FRAGMENT_SHADER, source);

	program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	//glBindFragDataLocation(program, 0, "outColor");
	glLinkProgram(program);
	glUseProgram(program);
}

void cvToGL::clean()
{
	glDetachShader(program, vs);
	glDetachShader(program, fs);
	glDeleteShader(vs);
	glDeleteShader(fs);
	glDeleteProgram(program);
}

cvToGL::~cvToGL(void)
{
	clean();
	glfwTerminate();
}

void cvToGL::justDraw()
{
	glClearColor(0.3f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.3f);
		glVertex2f(-0.5f, 0.3f);
		glVertex2f(0.5f, 0.3f);
		glVertex2f(0.9f, 0.0f);
		glVertex2f(0.5f, -0.3f);
		glVertex2f(-0.5f, -0.3f);
	glEnd();
	glfwSwapBuffers(window);
	glfwPollEvents();
	std::this_thread::sleep_for(std::chrono::seconds(5));
}


void cvToGL::drawTexture(Mat frame)
{
	GLuint VBO;
	glGenBuffers(1, &VBO);

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	VideoCapture webcam(CV_CAP_ANY);
	//Mat im = imread("C:\\Users\\Datta\\Desktop\\woodenBox.jpg");
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	//glTexImage2D(GL_TEXTURE_2D, 0, 3, im.cols, im.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, im.data);
	//glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	//GLfloat vertices[] = {
	//	// Positions          // Colors           // Texture Coords
	//	0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,   // Top Right
	//	0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,   // Bottom Right
	//	-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   // Bottom Left
	//	-0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f    // Top Left 
	//};
	GLfloat vertices[] = {
		// Positions          // Colors           // Texture Coords
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,   // Top Right
		1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,   // Bottom Right
		-1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,   // Bottom Left
		-1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f    // Top Left 
	};
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	initShaders("C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\texture.vsh", "C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\texture.fsh");
	GLint posAttrib = glGetAttribLocation(program, "position");
	GLint color = glGetAttribLocation(program, "color");
	GLint texCoord = glGetAttribLocation(program, "texCoord");
	GLint ourTexture = glGetUniformLocation(program, "ourTexture");

	do 
	{
		webcam >> frame;
		if (frame.data)
		{
			//imshow("opencv", frame);
			glTexImage2D(GL_TEXTURE_2D, 0, 3, frame.cols, frame.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, frame.data);
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, 0);

			cvWaitKey(1);
		}
		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
		glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glVertexAttribPointer(texCoord, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
		glEnableVertexAttribArray(posAttrib);
		glEnableVertexAttribArray(color);
		glEnableVertexAttribArray(texCoord);

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBindTexture(GL_TEXTURE_2D, texture);
		glDrawArrays(GL_QUADS, 0, 4);
		glLoadIdentity();
		glRotatef(30.0f, 1.0f, 1.0f, 1.0f);
		glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.0f, 0.3f);
			glVertex3f(-0.5f, 0.3f, -0.5f);
			glVertex3f(0.5f, 0.3f, -0.5f);
			glVertex3f(0.9f, 0.0f, -0.5f);
			glVertex3f(0.5f, -0.3f, -0.5f);
			glVertex3f(-0.5f, -0.3f, -0.5f);
		glEnd();
		//testDraws(false);
		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (!glfwWindowShouldClose(window));
}

void cvToGL::testDrawsInit()
{
	initShaders("C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\positionShaderOne.vsh", "C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\colorShaderOne.fsh");
	posAttrib = glGetAttribLocation(program, "position");
	colorAttrib = glGetAttribLocation(program, "inColor");
	MatrixID = glGetUniformLocation(program, "MVP");
}

void cvToGL::testDraws(bool infLoop)
{
	glm::mat4 trans;
	trans = glm::translate(trans, glm::vec3(0.5f, 0.25f, 0.1f));
	//trans = glm::translate(trans, glm::vec3(0.5f, 0.0f, 0.01f));
	//trans = glm::scale(trans, glm::vec3(0.6f, 0.7f, 0.8f));
	trans = glm::rotate(trans, glm::radians(30.0f), glm::vec3(0.0, 0.0, -1.0));
	//trans = glm::rotate(trans, glm::radians(30.0f), glm::vec3(1.0, 0.0, 0.0));
	cout << "trans: " << endl << glm::to_string(trans) << endl;

	GLuint VAO;
	glGenVertexArrays(1, &VAO);

	GLfloat vertices[] = {
		0.16f, 0.233f, 1.0f, 0.0f, 0.0f,  // Top Right
		-0.16f, 0.233f, 1.0f, 1.0f, 1.0f,  // Top Left
		-0.16f, -0.233f, 1.0f, 1.0f, 1.0f, // Bottom Left
		0.16f, -0.233f, 1.0f, 1.0f, 1.0f  // Bottom Right
	};

	GLuint indices[] = {  // Note that we start from 0!
		0, 1, 3,   // First Triangle
		1, 2, 3    // Second Triangle
	};

	GLuint EBO;
	glGenBuffers(1, &EBO);
	GLuint VBO;
	glGenBuffers(1, &VBO);

	testDrawsInit();

	do
	{
		//draw();
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(posAttrib);

		glEnableVertexAttribArray(colorAttrib);
		glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &trans[0][0]);
		glBindVertexArray(0);

		glBindVertexArray(VAO);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glBindVertexArray(0);

		//glfwPollEvents();
		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (!glfwWindowShouldClose(window) && infLoop);

}