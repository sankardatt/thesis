#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <iostream>
#include <thread>
#include <string>
#include <fstream>
#include <malloc.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glm/ext.hpp"
#include <SOIL.h>

using namespace std;
using namespace cv;

class cvToGL
{
public:
	cvToGL();
	//cvToGL(Mat im);
	void testDraws(bool);
	void justDraw();
	void drawTexture(Mat frame);
	void placeModel(double[][4], Mat);
	~cvToGL();

private:
	void clean(void);
	GLuint compileShaders(GLuint, string&);
	void loadShaders(const char*, string&);
	void initShaders(const char*, const char*);
	void testDrawsInit();

public:
	Mat frame;

private:
	unsigned char* image;
	int texWidth, texHeight;

	GLFWwindow* window;
	GLint vs, fs, program;
	GLint  colorAttrib;
	GLint posAttrib, MatrixID;
};