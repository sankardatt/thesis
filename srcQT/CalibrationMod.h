//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
#pragma once

#include "UnicodeHelper.h"
#include "MatrixT.h"
//#include <PassPoint.h>
#include "ControlPointT.h"

#include <list>
#include <vector>
#include <fstream>
using namespace std;


class CalibrationMod
{
public:
	//!Name of calibration module.
	//!That's the kam-file name
	tstring m_strName;
	list<ControlPointT<double,2>> m_lstPass2D;
	list<ControlPointT<double, 2>> m_lstResiduals;
	list<ControlPointT<double, 3>> m_lstPass3D;
	
	vector<double*> m_vParameterRefs;
	vector<tstring> m_vParameterNames;

public:
	//! Standard constructor
	CalibrationMod(void);
	//! Destructor
	~CalibrationMod(void);

	//!Sets the calibration module name
	void setName(tstring strName);
	//!\return Vector to access calibration parameters
	const vector<double*>& getParameterRefs(){ return m_vParameterRefs; }

	//!Transformation from world frame to image frame
	//!by collinearity equations
	virtual ControlPointT<double,2> model(ControlPointT<double,3>& pass3D);

	//!Compute differences between control point coordinates found in image and image coordinates of projected corresponding 3-d control points using the current model.
	//!Resulting residuals are stored in m_lstResiduals.
	//!\return A pointer to m_lstResiduals.
	list<ControlPointT<double, 2>>* computeResiduals();

	//!Compute mean for Residual list.
	VectorT<double, 2> meanAbsResidualsXY();

	//!Copy pass2DList to m_pass2DList
	void setPass2DList(const list<ControlPointT<double, 2>> &pass2DList);

	//!\param fileName A ASCII-Coded text file containing the id and
	//! 2-d pixel coordinates for each passpoint.
	//!\param lstPass list to store 2-d passpoints
	//!\return number of passpoints
	size_t loadPass2DList(const TCHAR* fileName);
	static size_t loadPass2DList(const TCHAR* fileName, list<ControlPointT<double, 2>>* pPass2DList);

	//!\param fileName file to save the 2-d coordinates
	//!\param pPass2DList source of 2d coordinates, if NULL source is m_lstPass2D
	int savePass2DList(TCHAR* fileName, list<ControlPointT<double, 2>>* pPass2DList = NULL);


	//!\param fileName A ASCII-Coded text file containing the id and
	//! 3-d pixel coordinates for each passpoint.
	//!\param lstPass list to store 3-d passpoints
	//!\return number of passpoints
	size_t loadPass3DList(const TCHAR* fileName);

	//!\param source file for 3d coordinates
	//!\param pPass3DList Target list for 3d coordinates
	static size_t loadPass3DList(const TCHAR* fileName, list<ControlPointT<double, 3>>* pPass3DList);

	//!\param destination file for 3d coordinates
	//!\param pPass3DList source list for 3d coordinates
	static int savePass3DList(const TCHAR* fileName, list<ControlPointT<double, 3>>* pPass3DList);


	//!\return Pointer to pass2DList
	list<ControlPointT<double, 2>>* getPass2DList();
	//!\return Pointer to pass3DList
	list<ControlPointT<double, 3>>* getPass3DList();





	//////////////////////////////////////////////////////////////////////
	//Virtual methods
	//!Sets default values for parameters
	virtual void setParametersToDefault()=0;

	virtual void estimateParameters() = 0;
	
	//!Loading parameters from kam-file
	virtual int setupFromKAM(const TCHAR *fileName)=0;
	
	//!Save prameters to kam-file
	virtual void writeKAMFile(const TCHAR *fileName)=0;

	//!Transformation from world frame to image frame
	//!by collinearity equations
	virtual VectorT<double, 2> model(VectorT<double, 3>& p)=0;

	//! Creates a point on viewing ray in world coordinates
	virtual VectorT<double, 3> pix2world(VectorT<double, 2> &pix, double d = 1000)=0;

	//!Starts the calibration to compute unknown model parameters
	virtual void startCalib()=0;
};