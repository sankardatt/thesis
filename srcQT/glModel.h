#pragma once
#include "MatrixT.h"
#include <opencv2/highgui/highgui.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <iostream>
#include <thread>
#include <string>
#include <fstream>
#include <malloc.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glm/ext.hpp"
#include<glm/gtc/quaternion.hpp>
#include<glm/gtx/quaternion.hpp>
#include <SOIL.h>

#include <DetectCodedEllipsesDLL.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include "CalibrationModE.h"

#include <Eigen/LU>
#include <Eigen/SparseQR>
#include <Eigen/Core>

using namespace std;
using namespace cv;

class glModel
{
public:
    glModel(void);
    glModel(int, int);
	void dltHandler(double r[][4], VideoCapture webcam, int infLoop);
	void dltHandler();
    void opencvHandler(Mat HomographyMatrix, int infLoop);
    void interactionHandler(float, float, int);
	cv::Mat justDraw();
    ~glModel(void);

public:
    //testing methods
    void glmTest(void);
    void draw(void);

private:
	//these are for externals
	static bool myfunction(DCE::CodedMarker a, DCE::CodedMarker b);
	void reprojection(double r[][4], double * intrinsics, double x, double y, double z, double& u, double& v);
	void externInit();
	int externeParams(float& m_omega, float& m_phi, float& m_kappa, float& xTrans, float& yTrans, float& zTrans, int& up, Mat frame);

private:
	//these are for externals
	CalibrationModE camera;
	double intrinsics[4];
	DCE::CodedEllipsesDetector CED;
	Mat imageGray;
	list<ControlPointT<double, 3>> localPass3D;

private:
	glm::mat4 mvpMat(glm::mat4 initMat, float m_omega, float m_phi, float m_kappa, float xTrans, float yTrans, float zTrans);

private:
    void varInit();
	void varTexInit();
    void clean(void);
    GLuint compileShaders(GLuint, string&);
    void loadShaders(const char*, string&);
    void initShaders(const char*, const char*);
    Mat calModelView(Mat H);
    glm::mat4 cameraInverse();
    void cameraPoseFromHomography(Mat& H, Mat& pose);
	void quaternion(glm::mat4 initMat);

	//these are for interaction
	void interactionInit(int hueThres, int satThres);
	int locateCenter(Mat frame);
	
	//void addButton(int rows, int cols, int sizeX, int sizeY, char*, Mat frame);
	void addButton(int rows, int cols, Size box, char*, Mat frame);
	void addCallBack(int rows, int cols, Size box, Mat frame, float& scaleFactor, int(*fn)(float&));
	static int teleCallBack(float& scaleFactor);
	static int zoomCallBack(float& scaleFactor);

private:
	Mat frame;
	double quater[4];
	//these are for interaction
	Mat blurredImg, HsvIm;
	Mat trackedIm;
	Moments m;
	Point center;
	vector<double> forefinger;
	vector<double> forefingerLower = vector<double>(3, 100); //HSV (V)alue lower range
	vector<double> forefingerUpper = vector<double>(3, 210); //HSV (V)alue higher range

private:
    GLuint vs, fs, program;
    int w, h;
    glm::mat4 cvToGL;
    int texWidth, texHeight;
    GLint colorAttrib;
	GLint texCoord;
	GLint ourTexture;
    unsigned char* image;
    GLint posAttrib, MatrixID;
    GLFWwindow* window;
    //Mat pose;
};

