#pragma once
#include <iostream>
#include <fstream>

#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <opencv2\opencv.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\core\opengl_interop.hpp>

#include "PCClock.h"

#include "imReadBasics.h"
#include "utils.h"
#include "geometry.h"
#include "glDraws.h"
#include "glCenturai.h"
#include "camCalib.h"
#include "glModel.h"
#include "colorSegmentation.h"

#include <GL/glew.h>

#include "cvToGL.h"
//#include <GLFW/glfw3.h>
using namespace std;
using namespace cv;

//static void onOpenGL(void* param)
//{
//	ogl::Texture2D* pTex = static_cast<ogl::Texture2D*>(param);
//	glLoadIdentity();
//
//	glTranslated(0.0, 0.0, -1.0);
//
//	glRotatef(55, 1, 0, 0);
//	glRotatef(45, 0, 1, 0);
//	glRotatef(0, 0, 0, 1);
//
//	static const int coords[6][4][3] = {
//		{ { +1, -1, -1 }, { -1, -1, -1 }, { -1, +1, -1 }, { +1, +1, -1 } },
//		{ { +1, +1, -1 }, { -1, +1, -1 }, { -1, +1, +1 }, { +1, +1, +1 } },
//		{ { +1, -1, +1 }, { +1, -1, -1 }, { +1, +1, -1 }, { +1, +1, +1 } },
//		{ { -1, -1, -1 }, { -1, -1, +1 }, { -1, +1, +1 }, { -1, +1, -1 } },
//		{ { +1, -1, +1 }, { -1, -1, +1 }, { -1, -1, -1 }, { +1, -1, -1 } },
//		{ { -1, -1, +1 }, { +1, -1, +1 }, { +1, +1, +1 }, { -1, +1, +1 } }
//	};
//
//	glEnable(GL_TEXTURE_2D);
//	pTex->bind();
//	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//
//	//glTexImage2D(GL_TEXTURE_2D, 0, 3, pTex->cols, pTex->rows, 0, GL_RGB, GL_UNSIGNED_BYTE, pTex->data);
//
//	for (int i = 0; i < 6; ++i) {
//		glColor3ub(i * 20, 100 + i * 10, i * 42);
//		glBegin(GL_QUADS);
//		for (int j = 0; j < 4; ++j) {
//			glVertex3d(0.2 * coords[i][j][0], 0.2 * coords[i][j][1], 0.2 * coords[i][j][2]);
//		}
//		glEnd();
//	}
//}
//
//void testQT()
//{
//	namedWindow("OpenGL", WINDOW_OPENGL);
//	Mat img1 = imread("C:\\Users\\Datta\\Desktop\\original.jpg");
//	glModel model = glModel(img1.rows, img1.cols);
//	Mat mdImg = model.justDraw();
//	img1 = mdImg + img1;
//	//imshow("OpenGL", img1);
//	//int data = 7;
//	//cv::ogl::Texture2D tex();
//	ogl::Texture2D tex;
//	tex.copyFrom(img1);
//	//tex.copyFrom(img1);
//	setOpenGlContext("OpenGL");
//	setOpenGlDrawCallback("OpenGL", onOpenGL, &tex);
//	
//	//imshow("main1", img1);
//	for (;;)
//	{
//		imshow("OpenGL", img1);
//		tex.copyFrom(img1);
//		updateWindow("OpenGL");
//		int key = waitKey(40);
//		if ((key & 0xff) == 'e')
//			break;
//	}
//	cvWaitKey(0);
//	setOpenGlDrawCallback("OpenGL", 0, 0);
//	destroyAllWindows();
//}
//
//static void on_opengl(void* param)
//{
//	glLoadIdentity();
//
//	glTranslated(0.0, 0.0, -1.0);
//
//	glRotatef(55, 1, 0, 0);
//	glRotatef(45, 0, 1, 0);
//	glRotatef(0, 0, 0, 1);
//
//	static const int coords[6][4][3] = {
//		{ { +1, -1, -1 }, { -1, -1, -1 }, { -1, +1, -1 }, { +1, +1, -1 } },
//		{ { +1, +1, -1 }, { -1, +1, -1 }, { -1, +1, +1 }, { +1, +1, +1 } },
//		{ { +1, -1, +1 }, { +1, -1, -1 }, { +1, +1, -1 }, { +1, +1, +1 } },
//		{ { -1, -1, -1 }, { -1, -1, +1 }, { -1, +1, +1 }, { -1, +1, -1 } },
//		{ { +1, -1, +1 }, { -1, -1, +1 }, { -1, -1, -1 }, { +1, -1, -1 } },
//		{ { -1, -1, +1 }, { +1, -1, +1 }, { +1, +1, +1 }, { -1, +1, +1 } }
//	};
//
//	for (int i = 0; i < 6; ++i) {
//		glColor3ub(i * 20, 100 + i * 10, i * 42);
//		glBegin(GL_QUADS);
//		for (int j = 0; j < 4; ++j) {
//			glVertex3d(0.2 * coords[i][j][0], 0.2 * coords[i][j][1], 0.2 * coords[i][j][2]);
//		}
//		glEnd();
//	}
//}
//
//int mainOpenCV(void)
//{
//	VideoCapture webcam(CV_CAP_ANY);
//
//	namedWindow("window", CV_WINDOW_OPENGL);
//
//	Mat frame;
//	ogl::Texture2D tex;
//
//	setOpenGlDrawCallback("window", on_opengl, &tex);
//
//	while (waitKey(30) < 0)
//	{
//		webcam >> frame;
//		cout << "Image:" << frame.step / frame.elemSize() << endl;
//		tex.copyFrom(frame);
//		updateWindow("window");
//	}
//
//	setOpenGlDrawCallback("window", 0, 0);
//	destroyAllWindows();
//
//	return 0;
//}
//
//void imgAdd()
//{
//	namedWindow("OpenGL", CV_WINDOW_AUTOSIZE);
//	Mat img1 = imread("C:\\Users\\Datta\\Desktop\\original.jpg");
//	glModel model = glModel(img1.rows, img1.cols);
//	Mat mdImg = model.justDraw();
//	imshow("OpenGL", mdImg);
//	cvWaitKey(0);
//	Mat addedImg;
//	addWeighted(img1, 0.5, mdImg, 0.5, 0.0, addedImg);
//	imshow("OpenGL", addedImg);
//	cvWaitKey(0);
//}
//
//void test1()
//{
//	namedWindow("window", CV_WINDOW_OPENGL);
//	Mat img1 = imread("C:\\Users\\Datta\\Desktop\\original.jpg");
//	int data = 7;
//	setOpenGlDrawCallback("window", on_opengl, NULL);
//	while (true)
//	{
//		imshow("window", img1);
//		cvWaitKey(1);
//		setOpenGlDrawCallback("window", on_opengl, NULL);
//		updateWindow("window");
//		cvWaitKey(1);
//	}
//}

//void on_opengl(void* userdata);
//
//int main(void)
//{
//	//VideoCapture webcam(CV_CAP_ANY);
//	Mat frame = imread("C:\\Users\\Datta\\Desktop\\original.jpg");
//
//	namedWindow("window", CV_WINDOW_OPENGL);
//
//	Mat frame;
//	ogl::Texture2D tex;
//
//	setOpenGlDrawCallback("window", on_opengl, &tex);
//
//	while (waitKey(30) < 0)
//	{
//		//webcam >> frame;
//		tex.copyFrom(frame);
//		updateWindow("window");
//	}
//
//	setOpenGlDrawCallback("window", 0, 0);
//	destroyAllWindows();
//
//	return 0;
//}
//
//void on_opengl(void* userdata)
//{
//	ogl::Texture2D* pTex = static_cast<ogl::Texture2D*>(userdata);
//	if (pTex->empty())
//		return;
//
//	glLoadIdentity();
//
//	glTranslated(0.0, 0.0, 1.0);
//
//	glRotatef(55, 1, 0, 0);
//	glRotatef(45, 0, 1, 0);
//	glRotatef(0, 0, 0, 1);
//
//	static const int coords[6][4][3] = {
//		{ { +1, -1, -1 }, { -1, -1, -1 }, { -1, +1, -1 }, { +1, +1, -1 } },
//		{ { +1, +1, -1 }, { -1, +1, -1 }, { -1, +1, +1 }, { +1, +1, +1 } },
//		{ { +1, -1, +1 }, { +1, -1, -1 }, { +1, +1, -1 }, { +1, +1, +1 } },
//		{ { -1, -1, -1 }, { -1, -1, +1 }, { -1, +1, +1 }, { -1, +1, -1 } },
//		{ { +1, -1, +1 }, { -1, -1, +1 }, { -1, -1, -1 }, { +1, -1, -1 } },
//		{ { -1, -1, +1 }, { +1, -1, +1 }, { +1, +1, +1 }, { -1, +1, +1 } }
//	};
//
//	glEnable(GL_TEXTURE_2D);
//	pTex->bind();
//
//	for (int i = 0; i < 6; ++i) {
//		glColor3ub(i * 20, 100 + i * 10, i * 42);
//		glBegin(GL_QUADS);
//		for (int j = 0; j < 4; ++j) {
//			glVertex3d(0.2*coords[i][j][0], 0.2 * coords[i][j][1], 0.2*coords[i][j][2]);
//		}
//		glEnd();
//	}
//}

void mainCt()
{
	glCenturai Ct = glCenturai();
	Ct.gameLoop();
}

void mainCS()
{
	colorSegmentation cS = colorSegmentation();
	cS.readFrame();
}

void mainDLT()
{
	glModel GM = glModel();
	GM.dltHandler();
}

void main()
{
	//mainCt();
	mainDLT();
}

void mainT()
{
	Mat im = imread("C:\\Users\\Datta\\Desktop\\original.jpg");
	//imshow("faf", im);
	//cvWaitKey(0);
	cvToGL CG = cvToGL();
	//CG.testDraws();
	//cout << "CG" << endl;
	//CG.justDraw();
	//CG.testDraws();
	//CG.drawTexture();
	
	//glModel gMod = glModel();
	//gMod.glmTest();

	//test1();
	//testQT();
	//imgAdd();
	//mainOpenCV();
	//randomTest();
}