//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
#pragma once
#include <iostream>
using namespace std;

#include <math.h>
#include <memory.h>
#include <stdarg.h>

//template <class T, size_t _m=1>
//class VectorT;

template <class T, size_t _r, size_t _c>
class MatrixT;
/*
typedef VectorT<char> Vectorc;
typedef VectorT<unsigned char> Vectoruc;
typedef VectorT<short> Vectors;
typedef VectorT<unsigned short> Vectorus;
typedef VectorT<int> Vectori;
typedef VectorT<unsigned int> Vectorui;
typedef VectorT<float> Vectorf;
typedef VectorT<double> Vectord;
*/


//!@brief VectorT<T,_m>template class
//!
//! This class represents a m-dimensional vector.
template<class T, size_t _m=1>
class VectorT
{
	
protected:
	//! The data
	//T* m_values; //heap version
#pragma warning(push)
#pragma warning(disable : 4200) // warning C4200: nonstandard extension used : zero-sized array in struct/union
	T m_values[_m];//stack version 
#pragma warning(pop)

public:
	//! Standard constructor
	VectorT(void)
	{
		//m_values=new T[_m];//Heap-Version
		//memset(m_values,0,_m*sizeof(T));
		fill(T(0));
	}

	//! Copy constructor
	//! is called when VectorT<type> v2=v1;
	VectorT(const VectorT& v)
	{
		//m_values=new T[_m];//needed for heap version
		memcpy(m_values,v.m_values,_m*sizeof(T));
	}
	
	//! Deconstructor
	~VectorT(void)
	{
		//delete[] m_values; //needed for heap version
	}

	

	//! Constructor. Creates a vector from argument list
	//!\param ... argument list. Important be sure to use values from type T as arguments.
	VectorT(T v0, ...)
	{
		//m_values=new T[_m]; //heap version
		va_list vl;
		va_start(vl,v0);
		m_values[0]=v0;
		

		//double maxDoubleValue;
		//memset(&maxDoubleValue,255,sizeof(double));

		float floatValue;
		memset(&floatValue,0,sizeof(float));
		char* s=(char*)&floatValue+sizeof(float)-1;
		*s=(char)0xFF;


		int intValue;
		memset(&intValue,0,sizeof(int));
		s=(char*)&intValue+sizeof(int)-1;
		*s=(char)0xFF;

		T tValue;
		memset(&tValue,0,sizeof(T));
		s=(char*)&tValue+sizeof(T)-1;
		*s=(char)0xFF;


		if(tValue==floatValue)
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = (T)va_arg(vl,double);
			}
		}
		/*
		else if(tValue==intValue)
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = (T)va_arg(vl,int);
			}
		}
		*/
		else
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = va_arg(vl,T);
			}
		}

		va_end(vl);
	}

	//!Setting vector from value list.
	void set(T v0, ...)
	{
		//m_values=new T[_m]; //heap version
		va_list vl;
		va_start(vl,v0);
		m_values[0]=v0;
		

		//double maxDoubleValue;
		//memset(&maxDoubleValue,255,sizeof(double));

		float floatValue;
		memset(&floatValue,0,sizeof(float));
		char* s=(char*)&floatValue+sizeof(float)-1;
		*s=(char)0xFF;


		int intValue;
		memset(&intValue,0,sizeof(int));
		s=(char*)&intValue+sizeof(int)-1;
		*s=(char)0xFF;

		T tValue;
		memset(&tValue,0,sizeof(T));
		s=(char*)&tValue+sizeof(T)-1;
		*s=(char)0xFF;


		if(tValue==floatValue)
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = (T)va_arg(vl,double);
			}
		}
		/*
		else if(tValue==intValue)
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = (T)va_arg(vl,int);
			}
		}
		*/
		else
		{
			for (unsigned int i=1;i<_m;i++)
			{
				m_values[i] = va_arg(vl,T);
			}
		}

		va_end(vl);
	}
	

	//! Constructor. Creates a vector with "size" elements from array arr
	VectorT(T* arr)
	{
		//_m=size;
		//m_values=new T[_m];//heap version
		memcpy(m_values,arr,_m*sizeof(T));
	}

	void set2(const T& e1, const T& e2)
	{
		m_values[0]=e1;
		m_values[1]=e2;
	}

	void set3(const T& e1, const T& e2, const T& e3)
	{
		m_values[0]=e1;
		m_values[1]=e2;
		m_values[2]=e3;
	}

	void set4(const T& e1, const T& e2, const T& e3, const T& e4)
	{
		m_values[0]=e1;
		m_values[1]=e2;
		m_values[2]=e3;
		m_values[3]=e4;
	}

	//! Fills the vector with a constant value.
	VectorT<T,_m>& fill(const T& e)
	{
		for(T* p = m_values;p<&m_values[_m];p++)
		{
			*p=e;
		}
		return *this;
	}

	//!\return number of elements
	size_t getSize(){return _m;}

	//!\return vectors data
	T* getValues(){return m_values;}

	//!\return The squared vector length.
	T sqlength()
	{
		T t=T(0);
		T* p;
		for (p=m_values;p<&m_values[_m];p++)
		{
			t+=*p * *p;
		}
		return t;
	}

	//!\return The length of the vector.
	double length()
	{
		return sqrt(sqlength());
	}

	//! Normalizing the vector.
	//!\return 1 on success otherwise 0.
	VectorT<T,_m>& normalize()
	{
		T l=(T)length();
		if (l>(T)0)
		{
			for (T* p=m_values;p<&m_values[_m];p++)
			{
				*p=*p/l;
			}
			//return 1;
		}
		return *this;
		//return 0;
	}

	//!\return A normalized vector.
	VectorT<T,_m> getNormalized()
	{
		VectorT<T,_m> t;
		T l=(T)length();
		if (l>0)
		{
			T* p;
			T* q;
			for (p=m_values,q=t.m_values;p<&m_values[_m];p++,q++)
			{
				*q=*p/l;
			}
		}
		return t;
	}

	//!\param v The second vector.
	//!\return The dot product of two vectors
	T dot(const VectorT<T,_m>& v)
	{
		T t=0;
		T* p;
		T* q;
		for (p=m_values,q=(T*)v.m_values;p<&m_values[_m];p++,q++)
		{
			t=t+ *p * *q;
		}
		return t;
	}

	//! Computes the cross product. Note: The cross product is only defined in R^3.
	//!\param v The second vector.
	//!\return The cross product of two vectors.
	VectorT<T,_m> cross(VectorT<T,_m>& v)
	{
		VectorT<T,_m> t;
		if(_m>2)
		{
			t.m_values[0]=m_values[1]*v.m_values[2]-m_values[2]*v.m_values[1];
			t.m_values[1]=m_values[2]*v.m_values[0]-m_values[0]*v.m_values[2];
			t.m_values[2]=m_values[0]*v.m_values[1]-m_values[1]*v.m_values[0];
		}
		return t;
	}
	
	

	//Access grants
	//! Subscript operator
	T& operator[](size_t i)
	{
		return m_values[i];
	}
	//! Subscript operator
	const T& operator[](size_t i) const
	{
		return m_values[i];
	}

	//! The copy operator
	VectorT& operator=(const VectorT& v)
	{
		//delete[] m_values; //needed for heap version
		//m_values=new T[_m];//needed for heap version
		memcpy(m_values,v.m_values,_m*sizeof(T));
		return *this;
	}

	/*
	//! The copy operator for DynVectors
	VectorT<T,_m>& operator=(DynVectorT<T>& v)
	{
		//delete[] m_values; //needed for heap version
		//m_values=new T[_m];//needed for heap version
		memcpy(m_values,v.getValues(),_m*sizeof(T));
		return *this;
	}
	*/

	//! type cast operator
	template<class U>
	operator VectorT<U, _m>();

	//!unary minus operator
	//! Subtract a constant value from all elements.
	VectorT<T,_m> operator-()
	{
		VectorT<T,_m> t(_m);
		T* p;
		T* q;
		for (p=m_values, q=t.m_values;p<&m_values[_m];p++,q++)
		{
			*q=-*p;
		}
		return t;
	}

	//! Add a constant value to all elements.
	VectorT<T,_m>& operator+=(const T& e)
	{
		T* pEnd=&m_values[_m];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p+=e;
		}
		return *this;
	}

	//! Addition of vectors.
	VectorT<T,_m>& operator+=(const VectorT<T,_m>& v)
	{
		T* p=m_values;
		T* q=(T*)v.m_values;
		T* pEnd=&m_values[_m];
		for (;p<pEnd;p++,q++)
		{
			*p += *q;
		}
		return *this;
	}


	//! Subtract a constant value from all elements.
	VectorT<T,_m>& operator-=(const T& e)
	{
		T* pEnd=&m_values[_m];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p-=e;
		}
		return *this;
	}

	//! Subtraction of vectors.
	VectorT<T,_m>& operator-=(const VectorT<T,_m>& v)
	{
		T* p=m_values;
		T* q=(T*)v.m_values;
		T* pEnd=&m_values[_m];
		for (;p<pEnd;p++,q++)
		{
			*p -= *q;
		}
		return *this;
	}


	//! Multiply all elements by a constant.
	VectorT<T,_m>& operator*=(const T& e)
	{
		T* pEnd=&m_values[_m];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p*=e;
		}
		return *this;
	}

	//! Devide all elements by a constant.
	VectorT<T,_m>& operator/=(const T& e)
	{
		T* pEnd=&m_values[_m];
		for (T* p=m_values;p<pEnd;p++)
		{
			*p/=e;
		}
		return *this;
	}

	
	//! Add a constant value to all elements.
	VectorT<T,_m> operator+(const T& e)
	{
		VectorT<T,_m> t;
		T* p;
		T* q;
		for (p=m_values, q=t.m_values;p<&m_values[_m];p++,q++)
		{
			*q=*p+e;
		}
		return t;
	}
	
	//! Addition of two vectors.
	VectorT<T,_m> operator+(const VectorT<T,_m>& v) const
	{
		VectorT<T,_m> t;
		for(int i=0;i<_m;i++)
		{
			t.m_values[i]=m_values[i]+v.m_values[i];
		}
		return t;
	}

	/*
	//! Addition of two vectors.
	VectorT<T,_m> operator+(const VectorT<T,_m>& v)
	{
		VectorT<T,_m> t;
		T* p;
		T* q;
		T* r;
		for (p=m_values, q=(T*)v.m_values, r=t.m_values;p<&m_values[_m];p++,q++,r++)
		{
			*r=*p+*q;
		}
		return t;
	}
	*/
	
	//! Subtract a constant value from all elements.
	VectorT<T,_m> operator-(const T& e)
	{
		VectorT<T,_m> t(_m);
		T* p;
		T* q;
		for (p=m_values, q=t.m_values;p<&m_values[_m];p++,q++)
		{
			*q=*p-e;
		}
		return t;
	}

	/*
	//! Subtraction of two vectors.
	VectorT<T,_m> operator-(const VectorT<T,_m>& v)
	{
		VectorT<T,_m> t;
		T* p;
		T* q;
		T* r;
		
		for (p=m_values, q=(T*)v.m_values, r=t.m_values;p<&m_values[_m];p++,q++,r++)
		{
			*r=*p-*q;
		}
		
		return t;
	}
	*/

	//! Subtraction of two vectors.
	VectorT<T,_m> operator-(const VectorT<T,_m>& v) const
	{
		VectorT<T,_m> t;
		for(int i=0;i<_m;i++)
		{
			t.m_values[i]=m_values[i]-v.m_values[i];
		}
		return t;
	}



	//! Multiply by a constant.
	VectorT<T,_m> operator*(const T& e)
	{
		VectorT<T,_m> t;
		T* p;
		T* q;
		for (p=m_values, q=t.m_values;p<&m_values[_m];p++,q++)
		{
			*q=*p*e;
		}
		return t;
	}

	//!@brief Operator multiplies element by element
	VectorT<T,_m> operator*(VectorT<T,_m> &v)
	{
		VectorT<T,_m> vec;
		T* p;
		T* q;
		T* r;
		for (p=m_values, q=(T*)v.m_values, r=vec.m_values;p<&m_values[_m];p++,q++,r++)
		{
			*r=*p * *q;
		}

		return vec;
	}

	//! Divide by a constant.
	VectorT<T,_m> operator/(const T& e)
	{
		VectorT<T,_m> t;
		T* p;
		T* q;
		for (p=m_values, q=t.m_values;p<&m_values[_m];p++,q++)
		{
			*q=*p/e;
		}
		return t;
	}
	
	//! Standard output
	template<class T>
	friend std::ostream& operator<<(std::ostream &s, const VectorT<T,_m> &v);

	//template<class T, size_t _m>
	//void MatrixT<T>::setColumn(size_t j, VectorT<T,_m>& v);
};


//Type Cast operator
template<class T,size_t _m> template<class U>
VectorT<T,_m>::operator VectorT<U,_m>()
{
	//U* arr=new U[_m]; ////needed for heap version
	U arr[_m];
	U* p;
	T* q;
	for (p=arr,q=m_values;p<&arr[_m];p++,q++)
	{
		*p=*q;
	}
	VectorT<U,_m> v(arr);
	//delete[] arr; ////needed for heap version
	return v;
}

template<class T,size_t size>
std::ostream& operator<< (std::ostream& s, VectorT<T,size> &v)
{
	s << "Vector:\n";
	for (T *p=v.getValues(); p<v.getValues()+size; p++)
	{
		s << *p <<"\n";
	}
	return s;
}

template<class T,size_t size>
VectorT<T,size> operator*(const T c, VectorT<T,size> &v)
{
	return v*c;
}
