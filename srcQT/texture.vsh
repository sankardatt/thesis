#version 150
in vec3 position;
in vec3 color;
in vec2 texCoord;

out vec3 ourColor;
out vec2 TexCoord;

void main()
{
    ourColor = color;
    TexCoord = texCoord;
	gl_Position = vec4(position, 1.0f);
}