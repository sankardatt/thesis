//#define GLEW_STATIC
#include "glModel.h"


glModel::glModel(void)
{
    glfwInit();
	w = 800;
	h = 600;
    window = glfwCreateWindow(w, h, "OpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    cout << "Created Window" <<endl;
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    
    if(err!=GLEW_OK)
        cout << "glewInit failed, aborting. Code " << err << ". " << endl;
    glViewport(0, 0, 800, 600);
    varInit();
}

glModel::glModel(int width, int height)
{
    w = width;
    h = height;
    glfwInit();
    window = glfwCreateWindow(width, height, "OpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    cout << "Created Window" <<endl;
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    
    if(err!=GLEW_OK)
        cout << "glewInit failed, aborting. Code " << err << ". " << endl;
    glViewport(0, 0, width, height);
    varInit();
}

void glModel::varInit()
{

	//cvToGL[1][1] = -1;
	//cvToGL[2][2] = -1;
	//cout << "cvToGL: " << glm::to_string(cvToGL) << endl;
	//image = SOIL_load_image("C:\\Users\\Sankar\\Desktop\\original.jpg", &texWidth, &texHeight, 0, SOIL_LOAD_RGB);
	//initShaders("C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\augmented.vsh", "C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\augmented.fsh");
	initShaders("C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\positionShaderOne.vsh", "C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\colorShaderOne.fsh");
	posAttrib = glGetAttribLocation(program, "position");
	colorAttrib = glGetAttribLocation(program, "inColor"); 
	texCoord = glGetAttribLocation(program, "texCoord");
	MatrixID = glGetUniformLocation(program, "MVP");
	ourTexture = glGetUniformLocation(program, "ourTexture");
}

void glModel::varTexInit()
{

	initShaders("C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\texture.vsh", "C:\\Users\\Datta\\Desktop\\cmake3\\srcQT\\texture.fsh");
	posAttrib = glGetAttribLocation(program, "position");
	colorAttrib = glGetAttribLocation(program, "color");
	texCoord = glGetAttribLocation(program, "texCoord");
	ourTexture = glGetUniformLocation(program, "ourTexture");
}

GLuint glModel::compileShaders(GLuint mode, string& str)
{
    const GLchar* source= str.c_str();
    GLuint shader = glCreateShader(mode);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    char error[1000];
    glGetShaderInfoLog(shader, 1000, NULL, error);
	if ( strlen(error) != 0 )
	{
		cout << "Shader " << mode << " \n Compile message: " << error << endl;
	}
    return shader;
}

void glModel::loadShaders(const char* fn, string& str)
{
    ifstream inFile(fn);
    if(!inFile.is_open())
    {
        cout << "File " << fn << " could not be opened !!! inFile.is_open() is false" <<endl;
        return;
    }
    char tmp[300];
    while(!inFile.eof())
    {
        inFile.getline(tmp, 300);
        str = str + tmp;
        str = str + '\n';
    }
}

void glModel::initShaders(const char* vshader, const char* fshader)
{
    string source;
    loadShaders(vshader, source);
    vs = compileShaders(GL_VERTEX_SHADER, source);
    source = "";
    loadShaders(fshader, source);
    fs = compileShaders(GL_FRAGMENT_SHADER, source);

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    //glBindFragDataLocation(program, 0, "outColor");
    glLinkProgram(program);
    glUseProgram(program);
}

void glModel::clean()
{
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glDeleteProgram(program);
}

glModel::~glModel(void)
{
    clean();
	glfwTerminate();
}

void glModel::cameraPoseFromHomography(Mat& H, Mat& pose)
{
    pose = Mat::eye(3, 4, CV_32FC1);      // 3x4 matrix, the camera pose
    float norm1 = (float)norm(H.col(0));
    float norm2 = (float)norm(H.col(1));  
    float tnorm = (norm1 + norm2) / 2.0f; // Normalization value

    //Mat p1 = H.col(0);       // Pointer to first column of H
    //Mat p2 = pose.col(0);    // Pointer to first column of pose (empty)

    //cv::normalize(p1, p2);   // Normalize the rotation, and copies the column to pose
    cv::normalize(H.col(0), pose.col(0));
    //pose.col(0) = p2.col(0);
    //cout << "p2:" << p2 <<endl << "Pose:" << pose;
    //p1 = H.col(1);           // Pointer to second column of H
    //p2 = pose.col(1);        // Pointer to second column of pose (empty)

    //cv::normalize(p1, p2);   // Normalize the rotation and copies the column to pose
    cv::normalize(H.col(1), pose.col(1));
    Mat p1 = pose.col(0);
    Mat p2 = pose.col(1);

    Mat p3 = p1.cross(p2);   // Computes the cross-product of p1 and p2
    Mat c2 = pose.col(2);    // Pointer to third column of pose
    p3.copyTo(c2);       // Third column is the crossproduct of columns one and two

    pose.col(3) = H.col(2) / tnorm;  //vector t [R|t] is the last column of pose
    cout<< "Pose:" << pose << endl;
}

void glModel::glmTest()
{
    glm::mat4 trans;
    trans = glm::translate(trans, glm::vec3(0.5f, 0.25f, 0.1f));
    //trans = glm::translate(trans, glm::vec3(0.5f, 0.0f, 0.01f));
    //trans = glm::scale(trans, glm::vec3(0.6f, 0.7f, 0.8f));
    trans = glm::rotate(trans, glm::radians(30.0f), glm::vec3(0.0, 0.0, -1.0));
    //trans = glm::rotate(trans, glm::radians(30.0f), glm::vec3(1.0, 0.0, 0.0));
    cout << "trans: " << endl<< glm::to_string(trans) <<endl;

    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    
    GLfloat vertices[] = {
     0.16f,  0.233f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 1.0f, 1.0f, 1.0f  // Bottom Right
    };
    
    GLuint indices[] = {  // Note that we start from 0!
    0, 1, 3,   // First Triangle
    1, 2, 3    // Second Triangle
    };

    GLuint EBO;
    glGenBuffers(1, &EBO);
    GLuint VBO;
    glGenBuffers(1, &VBO);

    do
    {
        //draw();
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

        glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(posAttrib);

        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &trans[0][0]);
        glBindVertexArray(0);

        glBindVertexArray(VAO);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glBindVertexArray(0);

        //glfwPollEvents();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }while(!glfwWindowShouldClose(window));
    
}

void glModel::draw()
{
    // this function draws a plane for testing purpose

    while(!glfwWindowShouldClose(window))
    {
        glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
        //glClear(GL_COLOR_BUFFER_BIT );
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBegin(GL_QUADS);
            glColor3f(0.5f, 0.5f,0.0f);
            glVertex2f(0.16, 0.233);
            glColor3f(0.5f, 0.5f,0.0f);
            glVertex2f(0.16, -0.233);
            glColor3f(0.5f, 0.5f, 0.0f);
            glVertex2f(-0.16, -0.233);
            glColor3f(0.5f, 0.5f, 0.0f);
            glVertex2f(-0.16, 0.233);
        glEnd();

        //glBegin(GL_QUADS);
        //    glColor3f(0.3f, 0.0f,0.0f);
        //    glVertex3f(0.1, 0.06, 0.1);
        //    glColor3f(0.3f, 0.0f,0.0f);
        //    glVertex3f(-0.1, 0.06, 0.1);
        //    glColor3f(0.3f, 0.0f, 0.0f);
        //    glVertex3f(-0.1, -0.06, 0.1);
        //    glColor3f(0.3f, 0.0f,0.0f);
        //    glVertex3f(0.1, -0.06, 0.1);
        //glEnd();

        //glfwPollEvents();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}

glm::mat4 glModel::cameraInverse()
{
    glm::mat4 intrinsicsInv;

    double f_x = 623.7274584149; // Focal length in x axis
    double f_y = 1.0001525492*f_x; // Focal length in y axis (usually the same?)
    double c_x = 341.0843632075; // Camera primary point x
    double c_y = 232.7543939883; // Camera primary point y

    //Mat inverseCameraMatrix = (Mat_<double>(3, 3) << 1/f_x , 0 , -c_x/f_x , 0 , 1/f_y , -c_y/f_y, 0 , 0 , 1);
    intrinsicsInv[0][0] = 1/f_x;   intrinsicsInv[1][0] = 0;      intrinsicsInv[2][0] = -c_x/f_x;

    intrinsicsInv[0][1] = 0;       intrinsicsInv[1][1] = 1/f_y;  intrinsicsInv[2][1] = -c_y/f_y;

    intrinsicsInv[0][2] = 0;       intrinsicsInv[1][2] = 0;      intrinsicsInv[2][2] = 1;
    
    return intrinsicsInv;
}

Mat glModel::calModelView(Mat H)
{
    //glMatrixMode(GL_PROJECTION);
    //glLoadIdentity();

    // Camera parameters
    double f_x = 623.7274584149; // Focal length in x axis
    double f_y = 1.0001525492*f_x; // Focal length in y axis (usually the same?)
    double c_x = 341.0843632075; // Camera primary point x
    double c_y = 232.7543939883; // Camera primary point y

    double fovY = 1/(f_x/h * 2);
    double aspectRatio = w/h * f_y/f_x;
    double near = .1;  // Near clipping distance
    double far = 1000;  // Far clipping distance
    double frustum_height = near * fovY;
    double frustum_width = frustum_height * aspectRatio;

    double offset_x = (w/2 - c_x)/w * frustum_width * 2;
    double offset_y = (h/2 - c_y)/h * frustum_height * 2;
    // Build and apply the projection matrix
    //glFrustum(-frustum_width - offset_x, frustum_width - offset_x, -frustum_height - offset_y, frustum_height - offset_y, near, far);

    Mat inverseCameraMatrix = (Mat_<double>(3, 3) << 1/f_x , 0 , -c_x/f_x , 0 , 1/f_y , -c_y/f_y, 0 , 0 , 1);
    cout << inverseCameraMatrix;

    // Column vectors of homography
    Mat h1 = (Mat_<double>(3,1) << H.at<double>(0,0) , H.at<double>(1,0) , H.at<double>(2,0));
    Mat h2 = (Mat_<double>(3,1) << H.at<double>(0,1) , H.at<double>(1,1) , H.at<double>(2,1));
    Mat h3 = (Mat_<double>(3,1) << H.at<double>(0,2) , H.at<double>(1,2) , H.at<double>(2,2));

    Mat inverseH1 = inverseCameraMatrix * h1;
    // Calculate a length, for normalizing
    double lambda = sqrt(h1.at<double>(0,0)*h1.at<double>(0,0) + h1.at<double>(1,0)*h1.at<double>(1,0) + h1.at<double>(2,0)*h1.at<double>(2,0));

    Mat rotationMatrix; 
    Mat modelviewMatrix;

    if(!lambda==0)
    {
        lambda = 1/lambda;
        // Normalize inverseCameraMatrix
        inverseCameraMatrix.at<double>(0,0) *= lambda;
        inverseCameraMatrix.at<double>(1,0) *= lambda;
        inverseCameraMatrix.at<double>(2,0) *= lambda;
        inverseCameraMatrix.at<double>(0,1) *= lambda;
        inverseCameraMatrix.at<double>(1,1) *= lambda;
        inverseCameraMatrix.at<double>(2,1) *= lambda;
        inverseCameraMatrix.at<double>(0,2) *= lambda;
        inverseCameraMatrix.at<double>(1,2) *= lambda;
        inverseCameraMatrix.at<double>(2,2) *= lambda;

        // Column vectors of rotation matrix
        Mat r1 = inverseCameraMatrix * h1;
        Mat r2 = inverseCameraMatrix * h2;
        Mat r3 = r1.cross(r2);    // Orthogonal to r1 and r2
  
        // Put rotation columns into rotation matrix... with some unexplained sign changes
        rotationMatrix = (Mat_<double>(3,3) <<  r1.at<double>(0,0) , -r2.at<double>(0,0) , -r3.at<double>(0,0) ,
                                               -r1.at<double>(1,0) , r2.at<double>(1,0) , r3.at<double>(1,0) ,
                                               -r1.at<double>(2,0) , r2.at<double>(2,0) , r3.at<double>(2,0));
 
        // Translation vector T
        Mat translationVector = inverseCameraMatrix * h3;
        translationVector.at<double>(0,0) *= 1;
        translationVector.at<double>(1,0) *= -1;
        translationVector.at<double>(2,0) *= -1;
 
        //cout << endl << endl << "Rotation: " << endl << rotationMatrix << endl;
        //cout << endl << endl << "Translation: " << endl << translationVector << endl;

        SVD decomposed(rotationMatrix); // I don't really know what this does. But it works.
        rotationMatrix = decomposed.u * decomposed.vt;

        modelviewMatrix = (Mat_<double>(4,4) << rotationMatrix.at<double>(0,0), rotationMatrix.at<double>(0,1), rotationMatrix.at<double>(0,2), translationVector.at<double>(0,0),
                                                    rotationMatrix.at<double>(1,0), rotationMatrix.at<double>(1,1), rotationMatrix.at<double>(1,2), translationVector.at<double>(1,0),
                                                    rotationMatrix.at<double>(2,0), rotationMatrix.at<double>(2,1), rotationMatrix.at<double>(2,2), translationVector.at<double>(2,0),
                                                    0,0,0,1);
    }
    else
    {
        cout << "lambda is zero" <<endl;
        modelviewMatrix = (Mat_<double>(4,4) << 1.0, 0.0,0.0,0.0, 0.0,1.0,0.0,0.0, 0.0,0.0,1.0,0.0, 0.0,0.0,0.0,1.0); 
    }
    return modelviewMatrix;
}

void glModel::reprojection(double r[][4], double * intrinsics, double x, double y, double z, double& u, double& v)
{
	double denom;

	x = x - r[0][3];
	y = y - r[1][3];
	z = z - r[2][3];
	denom = (r[2][0] * x + r[2][1] * y + r[2][2] * z);
	u = intrinsics[2] - intrinsics[0] * ((r[0][0] * x + r[0][1] * y + r[0][2] * z) / denom);
	v = intrinsics[3] - intrinsics[1] * ((r[1][0] * x + r[1][1] * y + r[1][2] * z) / denom);

}

bool glModel::myfunction(DCE::CodedMarker a, DCE::CodedMarker b)
{
	return (a._id<b._id);
}

void glModel::externInit()
{
	intrinsics[0] = 1385.1031065100;
	intrinsics[1] = 1.0010932862 * intrinsics[0];
	intrinsics[2] = 320;// 1010.9266820960;
	intrinsics[3] = 240;// 524.0818496426;

	CED.setOptions(100,					//Number of searching rays
		28,					//length of rays
		1,					//1==Black on white 0==White on Black
		1,					//1==Double measurement 0==Single measurement
		10.0,				//Threshold for edge detection
		360,				//Number of samples to read code ring
		1,					//Edge detection Algorithm
		"Testfeld.lst");	//Path to code marker file

	camera.loadPass3DList(_T("passReal.cor")); //add to the location of exe //try to ignore reading file, instead store in Ram
	localPass3D = camera.m_lstPass3D;
	camera.m_lstPass3D.clear();
}

int glModel::externeParams(float& m_omega, float& m_phi, float& m_kappa, float& xTrans, float& yTrans, float& zTrans, int& up, Mat frame)
{

	ControlPointT<double, 2> tmp;
	cvtColor(frame, imageGray, CV_BGR2GRAY);
	cvWaitKey(1);

	CED.setBitmap(imageGray.data, imageGray.cols, imageGray.rows);
	camera.m_lstPass2D.empty();

	const int foundMax = 100;
	DCE::CodedMarker codeMarkerArr[foundMax] = { 0 };
	int found = CED.detect(codeMarkerArr, foundMax);

	if (found > 11)
	{
		std::sort(codeMarkerArr, &codeMarkerArr[found], myfunction);
		int i;
		camera.m_lstPass2D.clear();
		camera.m_lstPass3D = localPass3D;
		for (i = 0; i < found; i++)
		{
			if (codeMarkerArr[i]._id >= 1 && codeMarkerArr[i]._id <= 28)
			{
				tmp.id() = codeMarkerArr[i]._id;
				tmp.coord()[0] = codeMarkerArr[i]._x;
				tmp.coord()[1] = imageGray.rows - codeMarkerArr[i]._y;
				camera.m_lstPass2D.push_back(tmp);
			}

		}

		//this is a temporary fix to Axes problem | Axes Problem: The orientation we solve using DLT is related to the axes of the markers and not
		//in accordance with image axes, which in return causes problem in orienting the model in openGL
		if (codeMarkerArr[0]._y < codeMarkerArr[--i]._y)
		{
			up = -1;
		}
		else
		{
			up = 1;
		}
		try
		{
			camera.externs(intrinsics);

			MatrixT<double, 3, 3> m_mtxRi;
			m_mtxRi(0, 0) = camera.transformationMat[0][0];	m_mtxRi(0, 1) = camera.transformationMat[0][1]; m_mtxRi(0, 2) = camera.transformationMat[0][2];
			m_mtxRi(1, 0) = camera.transformationMat[1][0];	m_mtxRi(1, 1) = camera.transformationMat[1][1]; m_mtxRi(1, 2) = camera.transformationMat[1][2];
			m_mtxRi(2, 0) = camera.transformationMat[2][0];	m_mtxRi(2, 1) = camera.transformationMat[2][1]; m_mtxRi(2, 2) = camera.transformationMat[2][2];

			m_omega = atan(-(m_mtxRi.getValues())[5] / (m_mtxRi.getValues())[8]);
			m_phi = asin((m_mtxRi.getValues())[2]);
			m_kappa = atan(-(m_mtxRi.getValues())[1] / (m_mtxRi.getValues())[0]);
			cout << "Orientation: X axis: " << m_omega * 180 / 3.14159265 << " Y axis : " << m_phi * 180 / 3.14159265 << " Z axis : " << m_kappa * 180 / 3.14159265 << endl;
			Vector3d projCenter;
			projCenter(0) = camera.transformationMat[0][3];
			projCenter(1) = camera.transformationMat[1][3];
			projCenter(2) = camera.transformationMat[2][3];
			double u, v;
			double X = 94.0; //marker no 13 location in 3D
			double Y = 94.0; //marker no 13 location in 3D
			double Z = 0.0; //marker no 13 location in 3D
			reprojection(camera.transformationMat, intrinsics, X, Y, Z, u, v);

			xTrans = 2 * (u / frame.cols - 0.5);
			yTrans = 2 * (v / frame.rows - 0.5);
			zTrans = 0;
		}
		catch (const std::exception& ex)
		{
			cout << "Error at getting externs: " << ex.what() << endl;
			return -1;
		}
		return 1;
	}
	return -1;
}

void glModel::interactionInit(int hueThres=7, int satThres=15)
{
	FileStorage fs("hsvValues.xml", FileStorage::READ);
	fs["Forefinger"] >> forefinger;
	fs.release();
	forefingerLower.at(0) = forefinger.at(0) - hueThres;
	forefingerLower.at(1) = forefinger.at(1) - satThres;
	forefingerUpper.at(0) = forefinger.at(0) + hueThres;
	forefingerUpper.at(1) = forefinger.at(1) + satThres;
}

int glModel::zoomCallBack(float& scaleFactor)
{
	scaleFactor = (scaleFactor + 0.2) > 2 ? 2 : (scaleFactor + 0.2);
	return 1;
}

int glModel::teleCallBack(float& scaleFactor)
{
	scaleFactor = (scaleFactor - 0.2) < 0.5 ? 0.5 : (scaleFactor - 0.2);
	return 1;
}

void glModel::addCallBack(int x, int y, Size box, Mat frame, float& scaleFactor, int(*fn)(float&) )
{
	int limY = (y - box.height) < 0 ? 0 : (y - box.height);
	int limX = (x + box.width) > frame.cols ? frame.cols : (x + box.width);

	if (center.x <= limX & center.x > x)
	{
		if (center.y <= y & center.y > limY)
		{
			fn(scaleFactor);
		}
	}
}

void glModel::addButton(int x, int y, Size box, char* txt, Mat frame)
{
	int limY = (y - box.height) < 0 ? 0 : (y - box.height);
	int limX = (x + box.width) > frame.cols ? frame.cols : (x + box.width);

	for (int rows = limY; rows < y; rows++)
	{
		for (int cols = x; cols < limX; cols++)
		{
			frame.at<Vec3b>(Point(cols, rows)) = frame.at<Vec3b>(Point(cols, rows)) + Vec3b(90, 10, 90);
		}
	}
	putText(frame, txt, Point(x, y), FONT_HERSHEY_SIMPLEX, 1.0, Scalar(120, 120, 0), 2, 6, false); //Puttext positions the text oriented to bottom left corner
}

int glModel::locateCenter(Mat frame)
{
	GaussianBlur(frame, blurredImg, Size(9, 9), 3, 3);
	cvtColor(blurredImg, HsvIm, CV_BGR2HSV);
	inRange(HsvIm, forefingerLower, forefingerUpper, trackedIm);
	try
	{
		m = moments(trackedIm, false);
		center = Point2f(static_cast<float>(m.m10 / m.m00), static_cast<float>(m.m01 / m.m00));
		circle(frame, center, 4, Scalar(1, 200, 180));
	}
	catch (...)
	{
		cout << "\n Failed to track forefinger. \n";
		return -1;
	}

	return 1;
}

inline float SIGN(float x) { return (x >= 0.0f) ? +1.0f : -1.0f; }
inline float NORM(float a, float b, float c, float d) { return sqrt(a * a + b * b + c * c + d * d); }
void glModel::quaternion(glm::mat4 initMat)
{
	double q0, q1, q2, q3, r;

	double r11 = initMat[0][0];
	double r12 = initMat[0][1];
	double r13 = initMat[0][2];

	double r21 = initMat[1][0];
	double r22 = initMat[1][1];
	double r23 = initMat[1][2];

	double r31 = initMat[2][0];
	double r32 = initMat[2][1];
	double r33 = initMat[2][2];
	
	q0 = (r11 + r22 + r33 + 1.0f) / 4.0f;
	q1 = (r11 - r22 - r33 + 1.0f) / 4.0f;
	q2 = (-r11 + r22 - r33 + 1.0f) / 4.0f;
	q3 = (-r11 - r22 + r33 + 1.0f) / 4.0f;
	if (q0 < 0.0f) q0 = 0.0f;
	if (q1 < 0.0f) q1 = 0.0f;
	if (q2 < 0.0f) q2 = 0.0f;
	if (q3 < 0.0f) q3 = 0.0f;
	q0 = sqrt(q0);
	q1 = sqrt(q1);
	q2 = sqrt(q2);
	q3 = sqrt(q3);
	if (q0 >= q1 && q0 >= q2 && q0 >= q3) {
		q0 *= +1.0f;
		q1 *= SIGN(r32 - r23);
		q2 *= SIGN(r13 - r31);
		q3 *= SIGN(r21 - r12);
	}
	else if (q1 >= q0 && q1 >= q2 && q1 >= q3) {
		q0 *= SIGN(r32 - r23);
		q1 *= +1.0f;
		q2 *= SIGN(r21 + r12);
		q3 *= SIGN(r13 + r31);
	}
	else if (q2 >= q0 && q2 >= q1 && q2 >= q3) {
		q0 *= SIGN(r13 - r31);
		q1 *= SIGN(r21 + r12);
		q2 *= +1.0f;
		q3 *= SIGN(r32 + r23);
	}
	else if (q3 >= q0 && q3 >= q1 && q3 >= q2) {
		q0 *= SIGN(r21 - r12);
		q1 *= SIGN(r31 + r13);
		q2 *= SIGN(r32 + r23);
		q3 *= +1.0f;
	}
	else {
		printf("coding error\n");
	}
	r = NORM(q0, q1, q2, q3);
	q0 /= r;
	q1 /= r;
	q2 /= r;
	q3 /= r;

	quater[0] = q0;
	quater[1] = q1;
	quater[2] = q2;
	quater[3] = q3;
	cout << "q0: " << q0 << "q1: " << q1 << "q2: " << q2 << "q3: " << q3 << endl;
}

glm::mat4 glModel::mvpMat(glm::mat4 initMat, float m_omega, float m_phi, float m_kappa, float xTrans, float yTrans, float zTrans)
{
	glm::mat4 trans;
	trans = glm::translate(trans, glm::vec3(xTrans, yTrans, 0.0f));

	glm::mat4 model = glm::mat4();
	//making -1 to 1 axes to mm axes
	model[0][0] = 587.5;
	model[1][1] = 403.5;
	model[2][2] = 180;
	
	glm::mat3 rot = glm::mat3();
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			rot[j][i] = initMat[i][j];
		}
	}

	//glm::quat q = glm::toQuat(rot);
	rot = glm::inverse(rot);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	up = rot * up;
	//up = glm::rotate(up, m_kappa, glm::vec3(0.0f, 0.0f, -1.0f));
	
	glm::vec3 lookingAt = glm::vec3(0.0f, 0.0f, -initMat[2][3]);
	lookingAt = rot * lookingAt;
	/*lookingAt = glm::rotate(lookingAt, m_omega, glm::vec3(1.0f, 0.0f, 0.0f));
	lookingAt = glm::rotate(lookingAt, m_phi, glm::vec3(0.0f, 1.0f, 0.0f));
	lookingAt = lookingAt +glm::vec4(initMat[0][3], initMat[1][3], initMat[2][3], 1.0f);*/
	glm::vec3 target = glm::vec3(lookingAt.x, lookingAt.y, lookingAt.z);
	//glm::vec3 target = glm::vec3(0.0f, 0.0f, 0.0f);
	
	float modelSize = (float)sqrt(pow(2*0.16*587,2) + pow(2*0.233*403.5,2) + pow(0.5*180,2));
	
	glm::mat4 CameraMatrix = glm::lookAt(
		glm::vec3(initMat[0][3], initMat[1][3], initMat[2][3]), // the position of your camera, in world space
		//lookingAt,                             // where you want to look at, in world space
		target,
		glm::vec3(up.x, up.y, up.z)        // probably glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down, which can be great too
		);
	
	float dist = (float)sqrt(initMat[0][3] * initMat[0][3] + initMat[1][3] * initMat[1][3] + initMat[2][3] * initMat[2][3]);

	glm::mat4 projectionMatrix = glm::perspective(
		glm::radians(70.8f),         // The horizontal Field of View, in degrees : the amount of "zoom". Think "camera lens". Usually between 90� (extra wide) and 30� (quite zoomed in)
		4.0f / 3.0f,                 // Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
		dist - modelSize/2 - 10.0f,    // Near clipping plane. Keep as big as possible, or you'll get precision issues.
		dist + modelSize/2 + 50.0f     // Far clipping plane. Keep as little as possible.
		);
	return trans*projectionMatrix*CameraMatrix*model;
}

void glModel::dltHandler()
{
	VideoCapture webcam(CV_CAP_ANY);
	double r[4][4];
	int infLoop = 1;
	
	if (! webcam.isOpened())
	{
		cout << "Unable to find camera" << endl;
		cout << "Enter any to exit. ";
		cin.ignore();
		exit(0);
	}

	dltHandler(r, webcam, infLoop);
}

void glModel::dltHandler(double r[][4], VideoCapture webcam, int infLoop)
{
	MatrixT<double, 3, 3> m_mtxRi;
	m_mtxRi(0, 0) = r[0][0];	m_mtxRi(0, 1) = r[0][1]; m_mtxRi(0, 2) = r[0][2];
	m_mtxRi(1, 0) = r[1][0];	m_mtxRi(1, 1) = r[1][1]; m_mtxRi(1, 2) = r[1][2];
	m_mtxRi(2, 0) = r[2][0];	m_mtxRi(2, 1) = r[2][1]; m_mtxRi(2, 2) = r[2][2];

	float m_omega = atan(-(m_mtxRi.getValues())[5] / (m_mtxRi.getValues())[8]);
	float m_phi = asin((m_mtxRi.getValues())[2]);
	float m_kappa = atan(-(m_mtxRi.getValues())[1] / (m_mtxRi.getValues())[0]);

	float xTrans, yTrans, zTrans;

	glm::mat4 MVP;

	//for (int i = 0; i < 4; i++)
	//{
	//	for (int j = 0; j < 4; j++)
	//	{
	//		MVP[i][j] = (GLfloat)r[i][j];
	//	}
	//}

	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	GLuint VAO2;
	glGenVertexArrays(1, &VAO2);

	GLuint EBO;
	glGenBuffers(1, &EBO);
	GLuint EBO2;
	glGenBuffers(1, &EBO2);

	GLuint VBO;
	glGenBuffers(1, &VBO);
	GLuint VBO2;
	glGenBuffers(1, &VBO2);

	//VideoCapture webcam(CV_CAP_ANY);

	GLuint VAOtex;
	glGenVertexArrays(1, &VAOtex);
	GLuint VBOtex;
	glGenBuffers(1, &VBOtex);
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	//glBindTexture(GL_TEXTURE_2D, texture);
	GLfloat texVertices[] = {
		// Positions          // Colors           // Texture Coords
		1.0f, 1.0f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,   // Top Right
		1.0f, -1.0f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,   // Bottom Right
		-1.0f, -1.0f, 0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,   // Bottom Left
		-1.0f, 1.0f, 0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f    // Top Left 
	};


	GLfloat vertices[] = {
		0.16f, 0.233f, 0.0f, 0.5f, 0.4f, 0.0f,  // Top Right
		-0.16f, 0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
		-0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
		0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f  // Bottom Right
	};

	GLfloat lines[] = {
		0.16f, 0.233f, 0.0f, 0.7f, 0.6f, 0.2f,  // Top Right
		0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Bottom Right
		-0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
		-0.16f, 0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left

		0.16f, 0.233f, 0.5f, 1.0f, 0.0f, 0.0f,  // Top Right
		0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f,  // Bottom Right
		-0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f, // Bottom Left
		-0.16f, 0.233f, 0.5f, 1.0f, 1.0f, 1.0f  // Top Left
	};

	GLuint indices[] = {  // Note that we start from 0!
		0, 1, 3, 4 };  // First Triangle
	//1, 2, 3    // Second Triangle
	//};
	GLuint lineIndices[] = {  // Note that we start from 0!
		0, 1, 2, 3,  // First Quad
		4, 5, 6, 7,
		0, 4, 7, 3,
		0, 1, 5, 4,
		1, 5, 6, 2,
		3, 2, 6, 7
		// Second Triangle
	};

	//MVP = glm::rotate(glm::mat4(), glm::radians(m_omega), glm::vec3(0.0, 1.0, 0.0));
	MVP = glm::rotate(glm::mat4(), m_omega, glm::vec3(-1.0, 0.0, 0.0));
	MVP = glm::rotate(MVP, m_phi, glm::vec3(0.0, 1.0, 0.0));
	MVP = glm::rotate(MVP, m_kappa, glm::vec3(0.0, 0.0, 1.0));
	int up = 1;
	int drawingDecision=0;
	//glBindTexture(GL_TEXTURE_2D, 0);
	glm::mat4 idn;

	glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	//double r[4][4];

	externInit();
	interactionInit();
	float scaleFactor = 1.0;
	glm::mat4 trns = glm::mat4();

	do
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		webcam >> frame;
		if (frame.data)
		{
			drawingDecision = externeParams(m_omega, m_phi, m_kappa, xTrans, yTrans, zTrans, up, frame);

			//MVP = glm::rotate(glm::mat4(), m_omega, glm::vec3(-1.0 , 0.0, 0.0));
			//MVP = glm::rotate(MVP, m_phi, glm::vec3(0.0, 1.0 , 0.0));
			//MVP = glm::rotate(MVP, m_kappa, glm::vec3(0.0, 0.0, 1.0));
			//MVP = glm::translate(MVP, glm::vec3(xTrans, yTrans, 0.0));


			varTexInit();
			
			glBindVertexArray(VAOtex);
			glBindBuffer(GL_ARRAY_BUFFER, VBOtex);
			glBufferData(GL_ARRAY_BUFFER, sizeof(texVertices), texVertices, GL_STATIC_DRAW);

			//locateCenter(frame);

			//addCallBack(frame.cols - 80, 140, Size(80, 40), frame, scaleFactor, teleCallBack);
			//addButton(frame.cols - 80, 140, Size(80, 40), "Tele", frame);

			//addCallBack(frame.cols - 80, 60, Size(90, 40), frame, scaleFactor, zoomCallBack);
			//addButton(frame.cols - 80, 60, Size(90, 40), "Zoom", frame);

			//MVP = glm::scale(MVP, glm::vec3(scaleFactor, scaleFactor, scaleFactor));
			
			glTexImage2D(GL_TEXTURE_2D, 0, 3, frame.cols, frame.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, frame.data);
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, 0);

			glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
			glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
			glVertexAttribPointer(texCoord, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));

			glEnableVertexAttribArray(posAttrib);
			glEnableVertexAttribArray(colorAttrib);
			glEnableVertexAttribArray(texCoord);

			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &idn[0][0]); //should be idn
			
			glBindVertexArray(0);

		}
		else
		{
			drawingDecision = 0;
		}

		glBindVertexArray(VAOtex);
		glBindTexture(GL_TEXTURE_2D, texture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindVertexArray(0);

		if (drawingDecision > 0)
		{
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					trns[i][j] = (GLfloat)camera.transformationMat[i][j];
				}
			}

			////quaternion(trns);
			////float a = sqrt(1 - quater[0]*quater[0]); //sin(theta/2) = sqrt(cos(theta/2))
			////MVP = glm::rotate(glm::mat4(), 2.0f * (float)acos(quater[0]), glm::vec3(quater[1] / a, quater[2] / a, quater[3] / a));
			////MVP = glm::rotate(glm::mat4(), (float)(quater[0]), glm::vec3(quater[1], quater[2], quater[3]));

			MVP = mvpMat(trns, m_omega, m_phi, m_kappa, xTrans, yTrans, zTrans);

			varInit();
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

			glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(posAttrib);

			glEnableVertexAttribArray(colorAttrib);
			glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			glBindVertexArray(0);

			glBindVertexArray(VAO2);
			glBindBuffer(GL_ARRAY_BUFFER, VBO2);
			glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lineIndices), lineIndices, GL_STATIC_DRAW);

			glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(posAttrib);

			glEnableVertexAttribArray(colorAttrib);
			glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			glBindVertexArray(0);

			glBindVertexArray(VAO);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
			//glClear(GL_COLOR_BUFFER_BIT);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);

			glBindVertexArray(VAO2);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //GL_LINE
			glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, 0);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glBindVertexArray(0);
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	} while ((!glfwWindowShouldClose(window)) && 1);

}

void glModel::opencvHandler(Mat HomographyMatrix, int infLoop)
{
    glm::mat4 MVP;
    //glm::vec3 cx, cy, cz;
    
    //glm::tvec3<double> cx, cy, cz;
    //cx.x = HomographyMatrix.at<double>(0,0);
    //cx.y = HomographyMatrix.at<double>(1,0);
    //cx.z = HomographyMatrix.at<double>(2,0);
    //cout<< "cX:"<< glm::to_string(cx)<< endl;
    //cy.x = HomographyMatrix.at<double>(0,1);
    //cy.y = HomographyMatrix.at<double>(1,1);
    //cy.z = HomographyMatrix.at<double>(2,1);
    //cout<< "cY:"<< glm::to_string(cy)<< endl;
    //cz = glm::cross(cx, cy);
    //cout<< "cZ:"<< glm::to_string(cz)<< endl;
    //
    ////cout  << cz.x << cz.y << cz.z << endl;

    ////first approach
    ////MVP[0][0] = HomographyMatrix.at<double>(0,0); MVP[1][0] = HomographyMatrix.at<double>(0,1); MVP[2][0] = HomographyMatrix.at<double>(0,2); MVP[3][0] = 0.0;

    ////MVP[0][1] = HomographyMatrix.at<double>(1,0); MVP[1][1] = HomographyMatrix.at<double>(1,1); MVP[2][1] = HomographyMatrix.at<double>(1,2); MVP[3][1] = 0.0;
    ////MVP[0][2] = cz.x;                             MVP[1][2] = cz.y;                             MVP[2][2] = cz.z;                             MVP[3][2] = 0;
    ////MVP[0][3] = HomographyMatrix.at<double>(2,0); MVP[1][3] = HomographyMatrix.at<double>(2,2); MVP[2][3] = -HomographyMatrix.at<double>(2,1); MVP[3][3] = 1;


    ////Second Approach is the Best guess
    //MVP[0][0] = HomographyMatrix.at<double>(0,0);   MVP[1][0] = -HomographyMatrix.at<double>(0,1);  MVP[2][0] = -cz.x;  MVP[3][0] =  2 * (HomographyMatrix.at<double>(0,2) - w/2) / w;

    //MVP[0][1] = -HomographyMatrix.at<double>(1,0);  MVP[1][1] = HomographyMatrix.at<double>(1,1);  MVP[2][1] = cz.y;   MVP[3][1] =  -2 * (HomographyMatrix.at<double>(1,2) - h/2) / h;
    //MVP[0][2] = -HomographyMatrix.at<double>(2,0);  MVP[1][2] = HomographyMatrix.at<double>(2,1);  MVP[2][2] = cz.z;   MVP[3][2] =   0 * (-HomographyMatrix.at<double>(2,2));
    //MVP[0][3] = 0;                                  MVP[1][3] = 0;                                 MVP[2][3] = 0;      MVP[3][3] = 1;

    //cout << "MVP:" <<endl << glm::to_string(MVP) <<endl;
    //MVP = cvToGL * MVP;

    //third approach
    //MVP[0][0] = HomographyMatrix.at<double>(0,0);
    //MVP[1][0] = cz.x;
    //MVP[2][0] = HomographyMatrix.at<double>(0,1);
    //MVP[3][0] = HomographyMatrix.at<double>(0,2);

    //MVP[0][1] = HomographyMatrix.at<double>(1,0);
    //MVP[1][1] = cz.y;
    //MVP[2][1] = HomographyMatrix.at<double>(1,1);
    //MVP[3][1] = HomographyMatrix.at<double>(2,2);

    //MVP[0][2] = HomographyMatrix.at<double>(2,0);
    //MVP[1][2] = cz.z;
    //MVP[2][2] = HomographyMatrix.at<double>(2,1);
    //MVP[3][2] = -HomographyMatrix.at<double>(1,2);

    //MVP[0][3] = 0;
    //MVP[1][3] = 0;
    //MVP[2][3] = 0;
    //MVP[3][3] = 1;

    //Fourth Approach
    //Mat pose;
    //cameraPoseFromHomography(HomographyMatrix, pose);
    //
    //MVP[0][0] = pose.at<float>(0,0); MVP[1][0] = pose.at<float>(0,1); MVP[2][0] = pose.at<float>(0,2); MVP[3][0] = pose.at<float>(0,3);

    //MVP[0][1] = -pose.at<float>(1,0); MVP[1][1] = -pose.at<float>(1,1); MVP[2][1] = -pose.at<float>(1,2); MVP[3][1] = pose.at<float>(1,3);
    //MVP[0][2] = -pose.at<float>(2,0); MVP[1][2] = -pose.at<float>(2,1); MVP[2][2] = -pose.at<float>(2,2); MVP[3][2] = pose.at<float>(2,3);
    //MVP[0][3] = 0;                   MVP[1][3] = 0;                   MVP[2][3] = 0;                   MVP[3][3] = 1;//pose.at<float>(3,3);
    //cout << "MVP"<<glm::to_string(MVP)<<endl;

    //MVP = glm::translate(MVP, glm::vec3(1.0f, 1.0f, 0.0f));
    //std::cout << vec.x << vec.y << vec.z << std::endl;

    //Fifth Approach
    Mat modelView = calModelView(HomographyMatrix);
    //cout << endl << endl << "modelView: " << endl << modelView << endl;

    MVP[0][0] = modelView.at<double>(0,0);  MVP[1][0] = modelView.at<double>(0,1);  MVP[2][0] = modelView.at<double>(0,2);  MVP[3][0] =  modelView.at<double>(0,3);

    MVP[0][1] = modelView.at<double>(1,0);  MVP[1][1] = modelView.at<double>(1,1);  MVP[2][1] = modelView.at<double>(1,2);  MVP[3][1] =  modelView.at<double>(1,3);

    MVP[0][2] = modelView.at<double>(2,0);  MVP[1][2] = modelView.at<double>(2,1);  MVP[2][2] = modelView.at<double>(2,2);  MVP[3][2] = 0 * modelView.at<double>(2,3);

    MVP[0][3] = modelView.at<double>(3,0);  MVP[1][3] = modelView.at<double>(3,1);  MVP[2][3] = modelView.at<double>(3,2);  MVP[3][3] =  modelView.at<double>(3,3);

    cout << "MVP:" <<endl << glm::to_string(MVP) <<endl;

    //Mine Fifth approach
/*    double lambda = sqrt(HomographyMatrix.at<double>(0,0)*HomographyMatrix.at<double>(0,0) + HomographyMatrix.at<double>(1,0)*HomographyMatrix.at<double>(1,0) + HomographyMatrix.at<double>(2,0)*HomographyMatrix.at<double>(2,0));
    glm::mat4 inv = cameraInverse();
    glm::mat4 homography =*/ 
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    GLuint VAO2;
    glGenVertexArrays(1, &VAO2);
    
    GLuint EBO;
    glGenBuffers(1, &EBO);
    GLuint EBO2;
    glGenBuffers(1, &EBO2);
    
    GLuint VBO;
    glGenBuffers(1, &VBO);
    GLuint VBO2;
    glGenBuffers(1, &VBO2);

     GLfloat vertices[] = {
     0.16f,  0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f  // Bottom Right
    };
    
     GLfloat lines[] = {
     0.16f,  0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Bottom Right

     0.16f,  0.233f, 0.5f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.5f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f  // Bottom Right
    };

    GLuint indices[] = {  // Note that we start from 0!
        0, 1, 3, 4};  // First Triangle
    //1, 2, 3    // Second Triangle
    //};
    GLuint lineIndices[] = {  // Note that we start from 0!
    0, 1, 2, 3,  // First Quad
    4, 5, 6, 7,
    0, 4, 7, 3,
    0, 1, 5, 4,
    1, 5, 6, 2,
    3, 2, 6, 7
    // Second Triangle
    };

    do
    {
        //draw();
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(posAttrib);

        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glBindVertexArray(0);

        glBindVertexArray(VAO2);
        glBindBuffer(GL_ARRAY_BUFFER, VBO2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lineIndices), lineIndices, GL_STATIC_DRAW);

        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(posAttrib);

        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glBindVertexArray(0);

        //glBindVertexArray(VAO);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
        //glClear(GL_COLOR_BUFFER_BIT);
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        ////glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        //glBindVertexArray(0);
        glBindVertexArray(VAO);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, 0);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glBindVertexArray(0);

        glBindVertexArray(VAO2);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //GL_LINE
        //glClear(GL_COLOR_BUFFER_BIT);
        glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, 0);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glBindVertexArray(0);

        //glfwPollEvents();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }while((!glfwWindowShouldClose(window)) && infLoop);
}

void glModel::interactionHandler(float yOrientation, float zOrientation, int infLoop)
{
    glm::mat4 MVP;
    //MVP = glm::rotate(MVP, glm::radians(yOrientation), glm::vec3(0.0, 1.0, 0.0));
    float y,z, angle;
    if (yOrientation > zOrientation)
    {
        angle = yOrientation;
        y = 1.0;
        z = zOrientation / yOrientation;
    }
    else
    {
        angle = zOrientation;
        z = 1.0;
        y = yOrientation / zOrientation;
    }
    MVP = glm::rotate(MVP, glm::radians(angle), glm::vec3(0.0, y, z));
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    GLuint VAO2;
    glGenVertexArrays(1, &VAO2);
    
    GLuint EBO;
    glGenBuffers(1, &EBO);
    GLuint EBO2;
    glGenBuffers(1, &EBO2);
    
    GLuint VBO;
    glGenBuffers(1, &VBO);
    GLuint VBO2;
    glGenBuffers(1, &VBO2);

     GLfloat vertices[] = {
     0.16f,  0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f  // Bottom Right
    };
    
     GLfloat lines[] = {
     0.16f,  0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Bottom Right

     0.16f,  0.233f, 0.5f, 1.0f, 0.0f, 0.0f,  // Top Right
    -0.16f,  0.233f, 0.5f, 1.0f, 1.0f, 1.0f,  // Top Left
    -0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f, // Bottom Left
     0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f  // Bottom Right
    };

    GLuint indices[] = {  // Note that we start from 0!
        0, 1, 3, 4};  // First Triangle
    //1, 2, 3    // Second Triangle
    //};
    GLuint lineIndices[] = {  // Note that we start from 0!
    0, 1, 2, 3,  // First Quad
    4, 5, 6, 7,
    0, 4, 7, 3,
    0, 1, 5, 4,
    1, 5, 6, 2,
    3, 2, 6, 7
    // Second Triangle
    };

    do
    {
        //draw();
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(posAttrib);

        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glBindVertexArray(0);

        glBindVertexArray(VAO2);
        glBindBuffer(GL_ARRAY_BUFFER, VBO2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lineIndices), lineIndices, GL_STATIC_DRAW);

        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(posAttrib);

        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glBindVertexArray(0);

        //glBindVertexArray(VAO);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
        //glClear(GL_COLOR_BUFFER_BIT);
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        ////glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        //glBindVertexArray(0);
        glBindVertexArray(VAO);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, 0);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glBindVertexArray(0);

        glBindVertexArray(VAO2);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //GL_LINE
        //glClear(GL_COLOR_BUFFER_BIT);
        glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, 0);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glBindVertexArray(0);

        //glfwPollEvents();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }while((!glfwWindowShouldClose(window)) && infLoop);
}


cv::Mat glModel::justDraw()
{
	glm::mat4 MVP;
	//MVP = glm::rotate(MVP, glm::radians(yOrientation), glm::vec3(0.0, 1.0, 0.0));
	float y, z, angle;
	angle = 30.0;
	y = 1;
	z = 0.5;

	MVP = glm::rotate(MVP, glm::radians(angle), glm::vec3(0.0, y, z));
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	GLuint VAO2;
	glGenVertexArrays(1, &VAO2);

	GLuint EBO;
	glGenBuffers(1, &EBO);
	GLuint EBO2;
	glGenBuffers(1, &EBO2);

	GLuint VBO;
	glGenBuffers(1, &VBO);
	GLuint VBO2;
	glGenBuffers(1, &VBO2);

	GLfloat vertices[] = {
		0.16f, 0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
		-0.16f, 0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
		-0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
		0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f  // Bottom Right
	};

	GLfloat lines[] = {
		0.16f, 0.233f, 0.0f, 1.0f, 0.0f, 0.0f,  // Top Right
		-0.16f, 0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Top Left
		-0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom Left
		0.16f, -0.233f, 0.0f, 1.0f, 1.0f, 1.0f,  // Bottom Right

		0.16f, 0.233f, 0.5f, 1.0f, 0.0f, 0.0f,  // Top Right
		-0.16f, 0.233f, 0.5f, 1.0f, 1.0f, 1.0f,  // Top Left
		-0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f, // Bottom Left
		0.16f, -0.233f, 0.5f, 1.0f, 1.0f, 1.0f  // Bottom Right
	};

	GLuint indices[] = {  // Note that we start from 0!
		0, 1, 3, 4 };  // First Triangle
	//1, 2, 3    // Second Triangle
	//};
	GLuint lineIndices[] = {  // Note that we start from 0!
		0, 1, 2, 3,  // First Quad
		4, 5, 6, 7,
		0, 4, 7, 3,
		0, 1, 5, 4,
		1, 5, 6, 2,
		3, 2, 6, 7
		// Second Triangle
	};

	cv::Mat img(w, h, CV_8UC3), flipped(w, h, CV_8UC3);
	glPixelStorei(GL_PACK_ALIGNMENT, (img.step & 3) ? 1 : 4);
	glPixelStorei(GL_PACK_ROW_LENGTH, img.step / img.elemSize());
	//do{
		//draw();
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(posAttrib);

		glEnableVertexAttribArray(colorAttrib);
		glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glBindVertexArray(0);

		glBindVertexArray(VAO2);
		glBindBuffer(GL_ARRAY_BUFFER, VBO2);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lineIndices), lineIndices, GL_STATIC_DRAW);

		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(posAttrib);

		glEnableVertexAttribArray(colorAttrib);
		glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glBindVertexArray(0);

		//glBindVertexArray(VAO);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
		//glClear(GL_COLOR_BUFFER_BIT);
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		////glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		//glBindVertexArray(0);
		glBindVertexArray(VAO);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_LINE
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, 0);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glBindVertexArray(0);

		glBindVertexArray(VAO2);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //GL_LINE
		//glClear(GL_COLOR_BUFFER_BIT);
		glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, 0);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBindVertexArray(0);

		//glfwPollEvents();
		glfwSwapBuffers(window);
		glfwPollEvents();
	//} while (!glfwWindowShouldClose(window));

	glReadPixels(0, 0, img.cols, img.rows, GL_BGR, GL_UNSIGNED_BYTE, img.data);
	cv::flip(img, flipped, 0);
	return flipped;
}