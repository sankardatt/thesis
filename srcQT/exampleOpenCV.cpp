//Be sure to link against:
// DetectCodedEllipsesDLLd.lib (Debug version) or DetectCodedEllipsesDLL.lib 
// The application must have access to DetectCodedEllipsesDLLd.dll (Debug version) or DetectCodedEllipsesDLL.dll

#include <DetectCodedEllipsesDLL.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

#include <stdio.h>
#include <tchar.h>
#include <iostream>
using namespace std;

#include "CalibrationModE.h"

//#include "cvToGL.h"
//#include "glModel.h"

void getExtern()
{
	_tchdir(_T("F:/Sankar/Master/Data/TestCalib/Faugeras"));
	CalibrationModE camera;
	//camera.readKamIntrinsicData();
	camera.loadPass2DList(_T("./iDSCam/idsImage2.DAT"));

	double intrinsics[4];
	intrinsics[0] = 3029.7551183989;
	intrinsics[1] = 1.0000348577 * intrinsics[0];
	intrinsics[2] = 1008.9700403982;
	intrinsics[3] = 1037.0500577392;
	camera.externs(intrinsics);
	getchar();
}

bool myfunction(DCE::CodedMarker a, DCE::CodedMarker b) 
{ 
	return (a._id<b._id); 
}

int mainT2(int argc, _TCHAR* argv[])
{
	//glModel GM = glModel();
	//cvToGL CG = cvToGL();
	CalibrationModE camera;
	size_t s = camera.loadPass3DList(_T("pass2.cor"));
	cout << "Size" << s << endl;
	double intrinsics[4];
	intrinsics[0] = 1385.1031065100;
	intrinsics[1] = 1.0010932862 * intrinsics[0];
	intrinsics[2] = 1010.9266820960;
	intrinsics[3] = 524.0818496426;
	VideoCapture webcam(CV_CAP_ANY);
	Mat frame;
	Mat image;
	//image = imread("A01.bmp", CV_LOAD_IMAGE_GRAYSCALE);   // Read the file
	//imshow("A01", image);
	//cvWaitKey(0);
	//Initialize Detector
	DCE::CodedEllipsesDetector::setOptions(100,					//Number of searching rays
		28,					//length of rays
		1,					//1==Black on white 0==White on Black
		1,					//1==Double measurement 0==Single measurement
		10.0,				//Threshold for edge detection
		360,				//Number of samples to read code ring
		1,					//Edge detection Algorithm
		"Testfeld.lst");	//Path to code marker file

	ControlPointT<double, 2> tmp;
	while (true)
	{
		webcam >> frame;
		
		if (frame.data)
		{
			cvtColor(frame, image, CV_BGR2GRAY);
			//imshow("gray", image);
			cvWaitKey(1);
			DCE::CodedEllipsesDetector::setBitmap(image.data, image.cols, image.rows);
			camera.m_lstPass2D.empty();
			//Detect markers in image (maximum 100)
			const int foundMax = 100;
			DCE::CodedMarker codeMarkerArr[foundMax] = { 0 };
			int found = DCE::CodedEllipsesDetector::detect(codeMarkerArr, foundMax);
			if (found > 11)
			{
				std::sort(codeMarkerArr, &codeMarkerArr[found], myfunction);
				camera.m_lstPass2D.clear();
				camera.loadPass3DList(_T("pass2.cor")); //add to the location of exe //try to ignore reading file, instead store in Ram
				for (int i = 0; i < found; i++)
				{
					cout << i << ")" << "\tid:" << codeMarkerArr[i]._id << "(" << codeMarkerArr[i]._x << ", " << codeMarkerArr[i]._y << ")" << "\tstdx: " << codeMarkerArr[i]._stdX << "\tstdY: " << codeMarkerArr[i]._stdY << endl;
					if (codeMarkerArr[i]._id >= 1 && codeMarkerArr[i]._id <= 28)
					{
						tmp.id() = codeMarkerArr[i]._id;
						tmp.coord()[0] = codeMarkerArr[i]._x;
						tmp.coord()[1] = codeMarkerArr[i]._y;
						camera.m_lstPass2D.push_back(tmp);
					}
					
				}
				try
				{
					camera.externs(intrinsics);
				}
				catch (const std::exception& ex)
				{
					cout << "Error at getting externs: " << ex.what() << endl;
				}

				try
				{
					//GM.dltHandler(camera.transformationMat, webcam, 0);
					imshow("frame", frame);
					//CG.drawTexture(frame);
					/*CG.frame = frame;
					CG.placeModel(camera.transformationMat, frame);*/
				}
				catch (const std::exception& ex)
				{
					cout << "Error in drawing: " << ex.what() << endl;
				}
			}
			
			//Detect a single marker on specified position
			/*DCE::CodedMarker codeMarker;
			int id = DCE::CodedEllipsesDetector::detectAt(407, 50, &codeMarker);
			if (id > 0)
			{
				cout << "Mark " << id << "detected on Position." << endl;
			}*/
		}
		//cin.ignore();
	}

	//getExtern();
	return 0;
}

