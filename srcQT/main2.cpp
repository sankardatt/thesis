#include <iostream>
#include <vector>

#include "CalibrationModE.h"

void getExtern2()
{
      _tchdir(_T("F:/Sankar/Master/Data/TestCalib/Faugeras"));
      CalibrationModE camera;
      //camera.readKamIntrinsicData();
      camera.loadPass2DList(_T("./iDSCam/idsImage2.DAT"));
      camera.loadPass3DList(_T("./iDSCam/pass.cor"));
	  
      double intrinsics[4];
      intrinsics[0] = 3029.7551183989;
      intrinsics[1] = 1.0000348577 * intrinsics[0];
      intrinsics[2] = 1008.9700403982;
      intrinsics[3] = 1037.0500577392;
      camera.externs(intrinsics);
      getchar();
}

//int main(int argc, char *argv[])
//int _tmain(int argc, _TCHAR* targv[])
//{
//	_tchdir(_T("C:/Users/Sankar/Desktop/Master/Data/TestCalib/Faugeras")); //set Working dir
//	CalibrationModE camera;
//	camera.loadPass2DList(_T("./cam01/A000000.dat"));//load control points found in image
//	camera.loadPass3DList(_T("pass.cor"));//load 3d ground truth points of calibration target
//	camera.dlt(); //perform DLT
//	camera.printParameters();
//	camera.writeKAMFile(_T("./cam01/camera_dlt.kam"));//output: write parameters to file
//	return 0;
//}

int  _tmain2(int argc, _TCHAR* targv[])
{
    getExtern2();
    return 0;
}