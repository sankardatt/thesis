#include "CalibrationModE.h"

//using namespace Eigen;

//#include "MathHelperFunc.h"
//#include "Intersection.h"

CalibrationModE::CalibrationModE(void)
{
	setParametersToDefault();
	//Parameters to calibrate
	m_vParameterNames.push_back(_T("cop_x\tx Component of Center of Projection"));
	m_vParameterRefs.push_back(&m_cop[0]);
	m_vParameterNames.push_back(_T("cop_y\ty Component of Center of Projection"));
	m_vParameterRefs.push_back(&m_cop[1]);
	m_vParameterNames.push_back(_T("cop_z\tz Component of Center of Projection"));
	m_vParameterRefs.push_back(&m_cop[2]);
	m_vParameterNames.push_back(_T("omega\tRotation about x-axis"));
	m_vParameterRefs.push_back(&m_omega);
	m_vParameterNames.push_back(_T("phi\tRotation about y-axis"));
	m_vParameterRefs.push_back(&m_phi);
	m_vParameterNames.push_back(_T("kappa\tRotation about z-axis"));
	m_vParameterRefs.push_back(&m_kappa);
	m_vParameterNames.push_back(_T("c\tCalibrated focal length"));
	m_vParameterRefs.push_back(&m_c);
	m_vParameterNames.push_back(_T("sy\tScaling parameter"));
	m_vParameterRefs.push_back(&m_sy);
	m_vParameterNames.push_back(_T("pp_x\tx Component of principal point"));
	m_vParameterRefs.push_back(&m_pp[0]);
	m_vParameterNames.push_back(_T("pp_y\ty Component of principal point"));
	m_vParameterRefs.push_back(&m_pp[1]);
	m_vParameterNames.push_back(_T("a1\tFirst radial distortion parameter"));
	m_vParameterRefs.push_back(&m_a1);
	m_vParameterNames.push_back(_T("a2\tSecond radial distortion parameter"));
	m_vParameterRefs.push_back(&m_a2);

}

CalibrationModE::~CalibrationModE(void)
{
}

void CalibrationModE::setParametersToDefault()
{
	m_dh = pow(numeric_limits<double>::epsilon() / 3.0, 1.0 / 3.0);//Constante to estimate difference quotient
	m_dh2=2*m_dh;

	m_cop=VectorT<double,3>(0.0,0.0,4000.0);
	m_omega=0.0;
	m_phi=0.0;
	m_kappa=0.0;
	m_c=1500.0;

	m_sy = -1.0;
	m_pp=VectorT<double,2>(300.0,200.0);
	set_r0(420);
	m_a1=0;
	m_a2=0;

	
}

void CalibrationModE::estimateParameters()
{
	setParametersToDefault();
	dlt();
}

void CalibrationModE::makeInvRotationMatrix()
{
	double com =cos(m_omega);
	double som =sin(m_omega);
	double cphi=cos(m_phi);
	double sphi=sin(m_phi);
	double ckap=cos(m_kappa);
	double skap=sin(m_kappa);
	
	double values[]={cphi*ckap,					-cphi*skap,					sphi,
				com*skap + som*sphi*ckap,	-som*sphi*skap + com*ckap,	-som*cphi,
				som*skap - com*sphi*ckap,	com*sphi*skap + som*ckap,	com*cphi};
	m_mtxRi.set(values);
	//cout << *m_pMtxR;
	int t=0;
}

void CalibrationModE::reprojection(Vector3d projCenter, double r[][3], double * intrinsics)
{
    list<ControlPointT<double, 3>>::iterator i3D = m_lstPass3D.begin();
    double x, y, z, denom, u, v;
    for(;i3D != m_lstPass3D.end(); i3D++)
    {
        x = (*i3D).coord()[0] - projCenter(0);
        y = (*i3D).coord()[1] - projCenter(1);
        z = (*i3D).coord()[2] - projCenter(2);
        denom = (r[2][0] * x + r[2][1]*y + r[2][2]*z);
        u = intrinsics[2] - intrinsics[0] * ( (r[0][0] * x + r[0][1]*y + r[0][2]*z) / denom);
        v = intrinsics[3] - intrinsics[1] * ( (r[1][0] * x + r[1][1]*y + r[1][2]*z)/ denom );
        std::cout << "ID " << (*i3D).id() << " U:" << u << " V:"<<v <<endl;
    }
}

int CalibrationModE::externs(double *intrinsics)
{
    //intrinsic defination: [focusX, focusY, centerX, centerY]
    double focusX = intrinsics[0];
    double focusY = intrinsics[1];
    double centerX = intrinsics[2];
    double centerY = intrinsics[3];
    //cout << "External parameter estimation" << endl;
	//Sort lists by PassPoint::_id ...
	m_lstPass2D.sort();
	m_lstPass3D.sort();
	//... and remove pass points from lstPass3D not contained in lstPass2D
	list<ControlPointT<double, 2>>::iterator i2D = m_lstPass2D.begin();
	list<ControlPointT<double, 3>>::iterator i3D = m_lstPass3D.begin();
    
    for(;i2D!=m_lstPass2D.end();i3D++)
	{
		if((*i2D).id()>(*i3D).id())
		{
			while((*i2D).id()!=(*i3D).id())
			{
				list<ControlPointT<double, 3>>::iterator i = i3D;
				i3D++;
				m_lstPass3D.remove(*i);
			}	
		}
		i2D++;
	}

    size_t n2D=m_lstPass2D.size();
	size_t n3D=m_lstPass3D.size();
	for(size_t i=0;i<n3D-n2D;i++)
	{
		m_lstPass3D.pop_back();                                       //need a better algo
	}
	
	i2D = m_lstPass2D.begin();
	i3D = m_lstPass3D.begin();
	int n=(int)m_lstPass2D.size();

    //////////////////////////////////////////////////////////
	//Solve linear equation system of form Ax=b, notation here is (mtxA*vL=vY)
	SparseMatrix<double> mtxA(n*2, 11);//rows x cols
	VectorXd vY(n*2);
    double U, V;
	for (int row = 0; i2D != m_lstPass2D.end(); i3D++, i2D++, row++)
	{
        U = (centerX - (*i2D).coord()[0]) / focusX;
        V = (centerY - (*i2D).coord()[1]) / focusY;

		mtxA.insert(row, 0) = U * (*i3D).coord()[0];
		mtxA.insert(row, 1) = U * (*i3D).coord()[1];
		mtxA.insert(row, 2) = U * (*i3D).coord()[2];
		mtxA.insert(row, 3) = -(*i3D).coord()[0];
		mtxA.insert(row, 4) = -(*i3D).coord()[1];
		mtxA.insert(row, 5) = -(*i3D).coord()[2];
        mtxA.insert(row, 9) = 1;
        vY(row) = U;
		
        row++;
		
        mtxA.insert(row, 0) = V * (*i3D).coord()[0];
		mtxA.insert(row, 1) = V * (*i3D).coord()[1];
		mtxA.insert(row, 2) = V * (*i3D).coord()[2];
		mtxA.insert(row, 6) = -(*i3D).coord()[0];
		mtxA.insert(row, 7) = -(*i3D).coord()[1];
		mtxA.insert(row, 8) = -(*i3D).coord()[2];
        mtxA.insert(row, 10) = 1;
        vY(row) = V;
	}
	//cout << mtxA;
	mtxA.makeCompressed();
	//SparseQR<SparseMatrix<double>, COLAMDOrdering<int> > solver;
	SparseQR<SparseMatrix<double>, NaturalOrdering<int> > solver;
	solver.compute(mtxA);
	if (solver.info() != Success)
	{
		// decomposition failed
		return -1;
	}

	VectorXd vL(n * 2);                                                //shouldn't it be a vector of length 11??
	vL = solver.solve(vY);
	if (solver.info() != Success) {
		// solving failed
		return -1;
	}
    double P = 1.0 / sqrt(vL(3) * vL(3) + vL(4) * vL(4) + vL(5) * vL(5));            //equation e
	/////////////////////////////////////////////////////
	//Orientation Matrix params
	//double transformationMat[3][3]; //r[row number][col number]
    transformationMat[0][0] = vL(3) * P;                                                            //equation f
    transformationMat[0][1] = vL(4) * P;
    transformationMat[0][2] = vL(5) * P;
    transformationMat[1][0] = vL(6) * P;
    transformationMat[1][1] = vL(7) * P;
    transformationMat[1][2] = vL(8) * P;
    transformationMat[2][0] = vL(0) * P;
    transformationMat[2][1] = vL(1) * P;
    transformationMat[2][2] = vL(2) * P;

	////////////////////////////////////////////////////
	//Translation Vector
	//Camera world co-ordinate aka Projection Center
	Matrix3d mtxCamCord;
	mtxCamCord << transformationMat[0][0], transformationMat[0][1], transformationMat[0][2],
        transformationMat[1][0], transformationMat[1][1], transformationMat[1][2],
        transformationMat[2][0], transformationMat[2][1], transformationMat[2][2];

	//Compute orientation angles from Matrix
	//Attention, there could be ambiguities!
	//m_mtxRi(0, 0) = transformationMat[0][0];	m_mtxRi(0, 1) = transformationMat[0][1]; m_mtxRi(0, 2) = transformationMat[0][2];
	//m_mtxRi(1, 0) = transformationMat[1][0];	m_mtxRi(1, 1) = transformationMat[1][1]; m_mtxRi(1, 2) = transformationMat[1][2];
	//m_mtxRi(2, 0) = transformationMat[2][0];	m_mtxRi(2, 1) = transformationMat[2][1]; m_mtxRi(2, 2) = transformationMat[2][2];

	//m_omega = atan(-(m_mtxRi.getValues())[5] / (m_mtxRi.getValues())[8]);
	//m_phi = asin((m_mtxRi.getValues())[2]);
	//m_kappa = atan(-(m_mtxRi.getValues())[1] / (m_mtxRi.getValues())[0]);

	Matrix3d mtxCamCordI;
    //Matrix3d mtxCamCordT = mtxCamCord.transpose(); //wasn't i using inverse ?
	try
	{
		mtxCamCordI = mtxCamCord.inverse();
	}
	catch (...)
	{
		cout << "Using transpose. Please bear with bad translation or show points with non zero z values" << endl;
		mtxCamCordI = mtxCamCord.transpose();
	}
	/*testing with inverse of rotation*/
	//transformationMat[0][0] = mtxCamCordI(0, 0);
	//transformationMat[0][1] = mtxCamCordI(0, 1);
	//transformationMat[0][2] = mtxCamCordI(0, 2);
	//transformationMat[1][0] = mtxCamCordI(1, 0);
	//transformationMat[1][1] = mtxCamCordI(1, 1);
	//transformationMat[1][2] = mtxCamCordI(1, 2);
	//transformationMat[2][0] = mtxCamCordI(2, 0);
	//transformationMat[2][1] = mtxCamCordI(2, 1);
	//transformationMat[2][2] = mtxCamCordI(2, 2);
	/*end of inverse*/
	//cout << "\n mat:" << mtxCamCord << "\n matT:" << mtxCamCordT << "\n matI:" << mtxCamCordI << endl;
	Vector3d v(vL(9)*P, vL(10)*P, P);
	
	Vector3d projCenter = mtxCamCordI*v;
	transformationMat[0][3] = projCenter(0);
	transformationMat[1][3] = projCenter(1);
	transformationMat[2][3] = projCenter(2);
	transformationMat[3][3] = 1;
    cout <<"\tX:" << projCenter(0) << "\tY:" << projCenter(1) << "\tZ:" << projCenter(2) <<endl;
    
	/*for(int i=0; i<3; i++)
    {
      std::cout << '\n';
      for(int j=0; j<3; j++)
           std::cout << r[i][j] <<" ";
    }
	
	cout << "End of solvePNP" << endl;
    reprojection(projCenter, r, intrinsics);*/

	//this part tries to convert mm to pixel and update the camera co-ordinates in pixel value
	//list<ControlPointT<double, 3>>::iterator first3D = m_lstPass3D.begin();
	//list<ControlPointT<double, 2>>::iterator first2D = m_lstPass2D.begin();
	//list<ControlPointT<double, 3>>::iterator last3D = m_lstPass3D.end();
	//list<ControlPointT<double, 2>>::iterator last2D = m_lstPass2D.end();
	//last2D--;
	//last3D--;
	//double x = (*last3D).coord()[0];
	//double y = (*last3D).coord()[1];
	//double z = (*last3D).coord()[2];
	//Vector3d lastPoint3D(x, y, z);
	//lastPoint3D = mtxCamCord * lastPoint3D;

	//x = (*first3D).coord()[0];
	//y = (*first3D).coord()[1];
	//z = (*first3D).coord()[2];
	//Vector3d firstPoint3D(x, y, z);
	//firstPoint3D = mtxCamCord * firstPoint3D;
	//double distance3D = sqrt(pow(lastPoint3D(0) - firstPoint3D(0), 2) + pow(lastPoint3D(1) - firstPoint3D(1), 2) + pow(lastPoint3D(2) - firstPoint3D(2), 2));
	//double distance2D = sqrt(pow((*last2D).coord()[0] - (*first2D).coord()[0], 2) + pow((*last2D).coord()[1] - (*first2D).coord()[1], 2));

	//transformationMat[0][3] = transformationMat[0][3] * distance2D / distance3D;
	//transformationMat[1][3] = transformationMat[1][3] * distance2D / distance3D;
	//transformationMat[2][3] = transformationMat[2][3] * distance2D / distance3D;

    return 1;
}


int CalibrationModE::dlt()
{
	cout << "Begin DLT for parameter estimation ..." << endl;
	//Sort lists by PassPoint::_id ...
	m_lstPass2D.sort();
	m_lstPass3D.sort();
	//... and remove pass points from lstPass3D not contained in lstPass2D
	list<ControlPointT<double, 2>>::iterator i2D = m_lstPass2D.begin();
	list<ControlPointT<double, 3>>::iterator i3D = m_lstPass3D.begin();
	for(;i2D!=m_lstPass2D.end();i3D++)
	{
		if((*i2D).id()>(*i3D).id())
		{
			while((*i2D).id()!=(*i3D).id())
			{
				list<ControlPointT<double, 3>>::iterator i = i3D;
				i3D++;
				m_lstPass3D.remove(*i);
			}	
		}
		i2D++;
	}
    size_t n2D=m_lstPass2D.size();
	size_t n3D=m_lstPass3D.size();
	for(size_t i=0;i<n3D-n2D;i++)
	{
		m_lstPass3D.pop_back();
	}

	
	i2D = m_lstPass2D.begin();
	i3D = m_lstPass3D.begin();
	int n=(int)m_lstPass2D.size(); //number of measurements
	

	//////////////////////////////////////////////////////////
	//Solve linear equation system of form Ax=b, notation here is (mtxA*vL=vY)
	SparseMatrix<double> mtxA(n*2, 11);//rows x cols                                     //Why Sparse Matrix??
	VectorXd vY(n*2);
	for (int row = 0; i2D != m_lstPass2D.end(); i3D++, i2D++, row++)
	{
		mtxA.insert(row, 0) = (*i3D).coord()[0];
		mtxA.insert(row, 1) = (*i3D).coord()[1];
		mtxA.insert(row, 2) = (*i3D).coord()[2];
		mtxA.insert(row, 3) = 1.0;
		mtxA.insert(row, 8) = -(*i2D).coord()[0] * (*i3D).coord()[0];
		mtxA.insert(row, 9) = -(*i2D).coord()[0] * (*i3D).coord()[1];
		mtxA.insert(row, 10) = -(*i2D).coord()[0] * (*i3D).coord()[2];
		vY(row) = (*i2D).coord()[0];
		row++;
		mtxA.insert(row, 4) = (*i3D).coord()[0];
		mtxA.insert(row, 5) = (*i3D).coord()[1];
		mtxA.insert(row, 6) = (*i3D).coord()[2];
		mtxA.insert(row, 7) = 1.0;
		mtxA.insert(row, 8) = -(*i2D).coord()[1] * (*i3D).coord()[0];
		mtxA.insert(row, 9) = -(*i2D).coord()[1] * (*i3D).coord()[1];
		mtxA.insert(row, 10) = -(*i2D).coord()[1] * (*i3D).coord()[2];
		vY(row) = (*i2D).coord()[1];
	}
	//cout << mtxA;
	mtxA.makeCompressed();
	//SparseQR<SparseMatrix<double>, COLAMDOrdering<int> > solver;
	SparseQR<SparseMatrix<double>, NaturalOrdering<int> > solver;
	solver.compute(mtxA);                                               //what's going on ??
	if (solver.info() != Success)
	{
		// decomposition failed
		return -1;
	}

	VectorXd vL(n * 2);                                                //shouldn't it be a vector of length 11??
	vL = solver.solve(vY);
	if (solver.info() != Success) {
		// solving failed
		return -1;
	}
	double l = -1.0 / sqrt(vL(8) * vL(8) + vL(9) * vL(9) + vL(10) * vL(10));        //l is D and this is equation 26
	/////////////////////////////////////////////////////
	//inner orientation
	//Principal point claculation
	m_pp[0] = l*l*(vL(0) * vL(8) + vL(1) * vL(9) + vL(2) * vL(10));                //equation 27
	m_pp[1] = l*l*(vL(4) * vL(8) + vL(5) * vL(9) + vL(6) * vL(10));
	//cout << "Principal point: (" << m_pp[0] << ", " <<m_pp[1] << ")" <<endl;

	//compute focal length
	double cx = sqrt(l*l*(vL(0) * vL(0) + vL(1) * vL(1) + vL(2) * vL(2)) - m_pp[0] * m_pp[0]);     //which equation ??
	double cy = sqrt(l*l*(vL(4) * vL(4) + vL(5) * vL(5) + vL(6) * vL(6)) - m_pp[1] * m_pp[1]);
	
	//adjust focal length, so it fits to our camera model definition
	m_c = -cx;
	m_sy = cy / m_c;

	////////////////////////////////////////////////////
	//outer orientation
	//Compute perspective center
	Matrix3d mtxH;
	mtxH << vL(0), vL(1), vL(2),
		vL(4), vL(5), vL(6),
		vL(8), vL(9), vL(10);
	
	Matrix3d mtxHi = mtxH.inverse()*-1.0;
	

	Vector3d v(vL(3), vL(7), 1.0);                                 // why all eigen library vectors are column vectors??
	
	Vector3d projCenter = mtxHi*v;                                 // Is v a column vector? It has to be, to sustain mtxHi(3x3) * v(3x1)
	m_cop.set3(projCenter(0), projCenter(1), projCenter(2));

	//Compute rotation matrix entries
	double r11 = l*(m_pp[0] * vL[8] - vL[0]) / cx; double r12 = l*(m_pp[1] * vL[8] - vL[4]) / -cy; double r13 = l*vL[8];   //solved from equation 9/ just that r indexes are reversed
	double r21 = l*(m_pp[0] * vL[9] - vL[1]) / cx; double r22 = l*(m_pp[1] * vL[9] - vL[5]) / -cy; double r23 = l*vL[9];
	double r31 = l*(m_pp[0] * vL[10] - vL[2]) / cx; double r32 = l*(m_pp[1] * vL[10] - vL[6]) / -cy; double r33 = l*vL[10];

	m_mtxRi(0, 0) = r11;	m_mtxRi(0, 1) = r21; m_mtxRi(0, 2) = r31;
	m_mtxRi(1, 0) = r12;	m_mtxRi(1, 1) = r22; m_mtxRi(1, 2) = r32;
	m_mtxRi(2, 0) = r13;	m_mtxRi(2, 1) = r23; m_mtxRi(2, 2) = r33;

	//Compute orientation angles from Matrix
	//Attention, there could be ambiguities!
	m_omega = atan(-(m_mtxRi.getValues())[5] / (m_mtxRi.getValues())[8]);
	m_phi = asin((m_mtxRi.getValues())[2]);
	m_kappa = atan(-(m_mtxRi.getValues())[1] / (m_mtxRi.getValues())[0]);

	//update rotation matrix from computed angles
	makeInvRotationMatrix();

	computeResiduals();
	VectorT<double, 2> meXY = meanAbsResidualsXY();
	//double mseRes = meXY.sqlength();
	cout << "absErrX: " << meXY[0] << ", absErrY: " << meXY[1] /*<< ", MSE: " << mseRes*/ << endl;
	
	cout << "End of DLT" << endl;
	return 1;
}




VectorT<double,2> CalibrationModE::model(VectorT<double,3>& p)
{
	VectorT<double,2> res;
	/*
	double* pR=m_mtxRi.getValues();
	double r11=pR[0];double r12=pR[3];double r13=pR[6];
	double r21=pR[1];double r22=pR[4];double r23=pR[7];
	double r31=pR[2];double r32=pR[5];double r33=pR[8];	
	
	double denom=r31*(p[0]-m_cop[0])+r32*(p[1]-m_cop[1])+r33*(p[2]-m_cop[2]);
	////undistorted Sensor coordinates
	//VectorT<double,2> undistSensor;
	//undistSensor[0]=m_c* ( (r11*(p[0]-m_cop[0])+r12*(p[1]-m_cop[1])+r13*(p[2]-m_cop[2]))/denom);
	//undistSensor[1]=m_c*( (r21*(p[0]-m_cop[0])+r22*(p[1]-m_cop[1])+r23*(p[2]-m_cop[2]))/denom);
		
	//////////////////////////////////////////////////
	//Colinearity equations without radial distortion
	res[0]=m_pp[0]+m_c* ( (r11*(p[0]-m_cop[0])+r12*(p[1]-m_cop[1])+r13*(p[2]-m_cop[2]))/denom);
	res[1]=m_pp[1]+m_c*m_sy*( (r21*(p[0]-m_cop[0])+r22*(p[1]-m_cop[1])+r23*(p[2]-m_cop[2]))/denom);
	//End Colinearity equations without radial distortion
	//////////////////////////////////////////////////////
	*/

	//Transformation from world frame to camera frame
	VectorT<double,3> cameraCoord=world2camera(p);

	//From camera frame to undistorted sensor frame
	VectorT<double,2> undistSensor=camera2undistSensor(cameraCoord);

	//include radial distortion
	VectorT<double,2> raddistSensor=distortRadial(undistSensor);
	
	//Compute unsigned y-scaled image coordinates
	res[0]=m_pp[0]+raddistSensor[0];
	res[1]=m_pp[1]+m_sy*raddistSensor[1];

	return res;
}


void CalibrationModE::startCalib()
{
	/*
	writeKAMFile(_T("paramsBeforeCalib.txt"));
	size_t nIterations=60;//number of iterations
	size_t bestIteration=0;
	double bestAbsMeanError=100;

	////////////////////////////////////////////////////
	//Adapt m_lstPass3D if some passpoint not detected
	//Sort lists by PassPoint::_id ...
	m_lstPass2D.sort();
	m_lstPass3D.sort();
	//... and remove pass points from lstPass3D not contained in lstPass2D
	list<ControlPointT<double, 2>>::iterator i2D = m_lstPass2D.begin();
	list<ControlPointT<double, 3>>::iterator i3D = m_lstPass3D.begin();
	for (; i2D != m_lstPass2D.end(); i3D++)
	{
		if ((*i2D).id()>(*i3D).id())
		{
			while ((*i2D).id() != (*i3D).id())
			{
				list<ControlPointT<double, 3>>::iterator i = i3D;
				i3D++;
				m_lstPass3D.remove(*i);
			}
		}
		i2D++;
	}
	size_t n2D = m_lstPass2D.size();
	size_t n3D = m_lstPass3D.size();
	for (size_t i = 0; i<n3D - n2D; i++)
	{
		m_lstPass3D.pop_back();
	}
	//////////////////////////////////////////////////////

	for(size_t i=0;i<nIterations;i++)
	{
		
		//Notes
		//In m_lstPass2D stehen die aktuellen Beobachtungen.
		//Der auszugleichende Parameter sei nur m_c. Eine Sch�tzung liegt schon als Startwert vor.
	
		//In lstModel2D stehen die Beobachtungen aus dem aktuellen Modell
		//list<ControlPointT<double, 2>> lstModel2D;

		//In pLlstShort2D stehen die gek�rzten Beobachtungen: found target points in image - projected target points by current model
		list<ControlPointT<double, 2>>* pLstShort2D=computeResiduals();
		VectorT<double, 2> meXY = meanAbsResidualsXY();
		double mseRes = meXY.sqlength();
		savePass2DList(_T("Beobachtungen.txt"));
		//savePass2DList(_T("ModellBeobachtungen.txt"),&lstModel2D);
		savePass2DList(_T("GekuerzteBeobachtungen.txt"), pLstShort2D);


		///////////////////////////////////////////////////////////////////////////////
		//Fehlerbeurteilung
		if (mseRes<bestAbsMeanError)
		{
			//bestN0=m_n0;
			bestIteration=i;
			bestAbsMeanError = mseRes;
		}

		cout << i << ")\t";
		printParameters();
		cout << i << ") absErrX: " << meXY[0] << ", absErrY: " << meXY[1] << ", MSE: " << mseRes << endl;



		//Erstelle die Design-Matrix
		size_t n=m_lstPass2D.size(); //Anzahl der Beobachtungen
		size_t u=12;					//Anzahl der Unbekannten (vorher 9, jetzt mit m_a1 und m_a2 und m_sy)
		DynMatrixT<double> mtxA(n * 2, u);
		
		DynVectorT<double> Y(n * 2);		//Vektor der gek�rzten Beobachtungen

		//list<ControlPointT<double, 2>>::iterator pShort = lstShort2D.begin();
		list<ControlPointT<double, 2>>::iterator pShort = pLstShort2D->begin();
		list<ControlPointT<double, 3>>::iterator pPass3D = m_lstPass3D.begin();

		//for(int row=0;pShort!=lstShort2D.end();pShort++,pPass3D++,row+=2)
		for (int row = 0; pShort != pLstShort2D->end(); pShort++, pPass3D++, row += 2)
		{
			//Berechne die Differenzenquotienten
			VectorT<double,2> dVec_c=dq_c(*pPass3D);			//Ableitung nach Kamerakonstante
			VectorT<double,2> dVec_ppx=dq_ppx(*pPass3D);		//Ableitung nach x-Komponente Bildhauptpunkt
			VectorT<double,2> dVec_ppy=dq_ppy(*pPass3D);		//Ableitung nach y-Komponente Bildhauptpunkt

			VectorT<double,2> dVec_copx=dq_copx(*pPass3D);	//Ableitung nach x-Komponente Projektionszentrum
			VectorT<double,2> dVec_copy=dq_copy(*pPass3D);	//Ableitung nach y-Komponente Projektionszentrum
			VectorT<double,2> dVec_copz=dq_copz(*pPass3D);	//Ableitung nach z-Komponente Projektionszentrum

			VectorT<double,2> dVec_omega=dq_omega(*pPass3D);
			VectorT<double,2> dVec_phi=dq_phi(*pPass3D);
			VectorT<double,2> dVec_kappa=dq_kappa(*pPass3D);

			VectorT<double,2> dVec_a1=dq_a1(*pPass3D);
			VectorT<double,2> dVec_a2=dq_a2(*pPass3D);

			VectorT<double,2> dVec_sy=dq_sy(*pPass3D);

			//Zeilen der Design-Matrix f�r die x- und y-Differenzequotienten
			DynVectorT<double> vrx(u, dVec_c[0], dVec_ppx[0], dVec_ppy[0], dVec_copx[0], dVec_copy[0], dVec_copz[0], dVec_omega[0], dVec_phi[0], dVec_kappa[0], dVec_a1[0], dVec_a2[0], dVec_sy[0]);
			DynVectorT<double> vry(u, dVec_c[1], dVec_ppx[1], dVec_ppy[1], dVec_copx[1], dVec_copy[1], dVec_copz[1], dVec_omega[1], dVec_phi[1], dVec_kappa[1], dVec_a1[1], dVec_a2[1], dVec_sy[1]);

			mtxA.setRow(row,vrx);
			mtxA.setRow(row+1,vry);

			//F�lle Y mit den gek�rzten Beobachtungen
			Y[row]=(*pShort).coord()[0];
			Y[row+1]=(*pShort).coord()[1];

			
		}

		//Der Vektor f�r die verk�rzten L�sungen der Unbekannten
		DynVectorT<double> L(u);
		//Bestimme die verk�rzten Unbekannten mittels der Methode der kleinsten Quadrate
		int r=math::leastSquares(mtxA,Y,L);
		double lenL = L.length();
		cout << "L length:" << lenL << endl;

		//Addiere die gek�rzten L�sungen zu den Sch�tzungen
		m_c=m_c+L[0];	
		m_pp[0]=m_pp[0]+L[1];
		m_pp[1]=m_pp[1]+L[2];
		m_cop[0]=m_cop[0]+L[3];
		m_cop[1]=m_cop[1]+L[4];
		m_cop[2]=m_cop[2]+L[5];
		m_omega=m_omega+L[6];
		m_phi=m_phi+L[7];
		m_kappa=m_kappa+L[8];
		m_a1=m_a1+L[9];
		m_a2=m_a2+L[10];
		m_sy=m_sy+L[11];

		makeRotationMatrix();//update rotation matrix
	}
	cout<<"Best iteration: " << bestIteration << ") absMeanErr: " << bestAbsMeanError  <<endl;
	writeKAMFile(_T("paramsAfterCalib.txt"));

	int j=0;
	*/
}

VectorT<double, 2> CalibrationModE::dq_c(ControlPointT<double, 3>& pass3D)
{
	////////////////////////////////////////////////////////
	//Numeric computation of differential quotient
	double old_c=m_c;
	m_c=old_c+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_c=old_c-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_c=old_c;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);

	/*
	//////////////////////////////////////////////////////////////////
	//Analytic computation of differential quotient 1'st partial deviation of m_c of used collinearity equation 
	//Be sure to use correct collinearity equations!
	double* pR=m_mtxRi.getValues();
	//double r11=pR[0];double r12=pR[3];double r13=pR[6];
	//double r21=pR[1];double r22=pR[4];double r23=pR[7];
	//double r31=pR[2];double r32=pR[5];double r33=pR[8];
	double denom=pR[2]*(pass3D._x-m_cop[0])+pR[5]*(pass3D._y-m_cop[1])+pR[8]*(pass3D._z-m_cop[2]);	
	return VectorT<double,2>((pR[0]*(pass3D._x-m_cop[0])+pR[3]*(pass3D._y-m_cop[1])+pR[6]*(pass3D._z-m_cop[2]))/denom,
		m_sy*( (pR[1]*(pass3D._x-m_cop[0])+pR[4]*(pass3D._y-m_cop[1])+pR[7]*(pass3D._z-m_cop[2]))/denom));
	*/
}

VectorT<double, 2> CalibrationModE::dq_ppx(ControlPointT<double, 3>& pass3D)
{
	////////////////////////////////////////////////////////
	//Numeric computation of differential quotient
	double old_ppx=m_pp[0];
	m_pp[0]=old_ppx+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_pp[0]=old_ppx-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_pp[0]=old_ppx;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);

	/*
	//////////////////////////////////////////////////////////////////
	//Analytic computation of differential quotient 1'st partial deviation of m_pp[0] of used collinearity equation 
	//Be sure to use correct collinearity equations!
	return VectorT<double,2>(1.0,0.0);
	*/
}

VectorT<double, 2> CalibrationModE::dq_ppy(ControlPointT<double, 3>& pass3D)
{
	////////////////////////////////////////////////////////
	//Numeric computation of differential quotient
	double old_ppy=m_pp[1];
	m_pp[1]=old_ppy+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_pp[1]=old_ppy-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_pp[1]=old_ppy;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);

	/*
	//////////////////////////////////////////////////////////////////
	//Analytic computation of differential quotient 1'st partial deviation of m_pp[1] of used collinearity equation 
	//Be sure to use correct collinearity equations!
	return VectorT<double,2>(0.0,1.0);
	*/
}

VectorT<double, 2> CalibrationModE::dq_copx(ControlPointT<double, 3>& pass3D)
{
	double old_copx=m_cop[0];
	m_cop[0]=old_copx+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_cop[0]=old_copx-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_cop[0]=old_copx;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_copy(ControlPointT<double, 3>& pass3D)
{
	double old_copy=m_cop[1];
	m_cop[1]=old_copy+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_cop[1]=old_copy-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_cop[1]=old_copy;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_copz(ControlPointT<double, 3>& pass3D)
{
	double old_copz=m_cop[2];
	m_cop[2]=old_copz+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_cop[2]=old_copz-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_cop[2]=old_copz;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/m_dh2,(dPlus.coord()[1]-dMinus.coord()[1])/m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_omega(ControlPointT<double, 3>& pass3D)
{
	double old_omega=m_omega;
	m_omega=old_omega+m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_omega=old_omega-m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_omega=old_omega;
	makeInvRotationMatrix();
	return VectorT<double, 2>((dPlus.coord()[0] - dMinus.coord()[0]) / m_dh2, (dPlus.coord()[1] - dMinus.coord()[1]) / m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_phi(ControlPointT<double, 3>& pass3D)
{
	double old_phi=m_phi;
	m_phi=old_phi+m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_phi=old_phi-m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_phi=old_phi;
	makeInvRotationMatrix();
	return VectorT<double, 2>((dPlus.coord()[0] - dMinus.coord()[0]) / m_dh2, (dPlus.coord()[1] - dMinus.coord()[1]) / m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_kappa(ControlPointT<double, 3>& pass3D)
{
	double old_kappa=m_kappa;
	m_kappa=old_kappa+m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_kappa=old_kappa-m_dh;
	makeInvRotationMatrix();
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_kappa=old_kappa;
	makeInvRotationMatrix();
	return VectorT<double, 2>((dPlus.coord()[0] - dMinus.coord()[0]) / m_dh2, (dPlus.coord()[1] - dMinus.coord()[1]) / m_dh2);
}

VectorT<double, 2> CalibrationModE::dq_sy(ControlPointT<double, 3>& pass3D)
{
	////////////////////////////////////////////////////////
	//Numeric computation of differential quotient
	double old_sy=m_sy;
	m_sy=old_sy+m_dh;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_sy=old_sy-m_dh;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_sy=old_sy;
	return VectorT<double, 2>((dPlus.coord()[0] - dMinus.coord()[0]) / m_dh2, (dPlus.coord()[1] - dMinus.coord()[1]) / m_dh2);
	/*
	//////////////////////////////////////////////////////////////////
	//Analytic computation of differential quotient 1'st partial deviation of m_sy of used collinearity equation 
	//Be sure to use correct collinearity equations!
	double* pR=m_mtxRi.getValues();
	//double r11=pR[0];double r12=pR[3];double r13=pR[6];
	//double r21=pR[1];double r22=pR[4];double r23=pR[7];
	//double r31=pR[2];double r32=pR[5];double r33=pR[8];
	double denom=pR[2]*(pass3D._x-m_cop[0])+pR[5]*(pass3D._y-m_cop[1])+pR[8]*(pass3D._z-m_cop[2]);	
	return VectorT<double,2>(0.0,
		m_c*( (pR[1]*(pass3D._x-m_cop[0])+pR[4]*(pass3D._y-m_cop[1])+pR[7]*(pass3D._z-m_cop[2]))/denom));
	*/
}

VectorT<double, 2> CalibrationModE::dq_a1(ControlPointT<double, 3>& pass3D)
{
	double old_a1=m_a1;
	double h=DBL_EPSILON;
	double h2=2*h;
	m_a1=old_a1+h;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_a1=old_a1-h;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_a1=old_a1;
	return VectorT<double,2>((dPlus.coord()[0]-dMinus.coord()[0])/h2,(dPlus.coord()[1]-dMinus.coord()[1])/h2);
}

VectorT<double, 2> CalibrationModE::dq_a2(ControlPointT<double, 3>& pass3D)
{
	double old_a2=m_a2;
	double h=DBL_EPSILON;
	double h2=2*h;
	m_a2=old_a2+h;
	ControlPointT<double, 2> dPlus = CalibrationMod::model(pass3D);
	m_a2=old_a2-h;
	ControlPointT<double, 2> dMinus = CalibrationMod::model(pass3D);
	m_a2=old_a2;
	return VectorT<double, 2>((dPlus.coord()[0] - dMinus.coord()[0]) / h2, (dPlus.coord()[1] - dMinus.coord()[1]) / h2);
}

VectorT<double,2> CalibrationModE::pix2undistSensor(VectorT<double,2> &pix)
{
	VectorT<double,2> dist=pix-m_pp;//distorted signed sensor coordinates
	dist[1]/=m_sy;					//with rescaling of y-coordinate

	VectorT<double,2> undist=dist;

	/////////////////////////////////////////////////
	//Correction of radial distortion
	if(m_a1==0 && m_a2==0) //There is no distortion
		return undist;

	double accuracy=0.000001;//accuracy is the abort criterium of Newton iteration
	double rd=sqrt(dist[0]*dist[0]+dist[1]*dist[1]);
	double r=rd;
	if(r==0)
		return undist;

	double r_2=0;
	double fr=0;
	double frd=0;

	int i=0;
	//Start Newton iteration
	for(;i<30;i++)	//Maximale Anzahl der Iterationen: 30
	{
		r_2=r*r;
		fr=r*(1.0+m_a1*(r_2-m_r0_2)+m_a2*(r_2*r_2-m_r0_4))-rd;
		
		if(abs(fr)<accuracy)
			break;
		//Computing the derivative of fr
		frd=1.0+m_a1*(3.0*r_2-m_r0_2)+m_a2*(5.0*r_2*r_2-m_r0_4);
		
		if(abs(frd)<accuracy)
			cout << "Caution: Division by Zero!"<<endl;
		r=r-fr/frd;//Applying iterative Newton formular to get a better solution for r
	}

	double ratio=r/rd;
	undist*=ratio;
	return undist;
	//End of correction
	//////////////////////////
	

	/*
	/////////////////////////////////////////
	//Old Version from Roman Calow
	if(m_a1!=0 || m_a2!=0)//is there distortion?
	{
		double dr=0;
		int iter = 5;
		double r2=m_r0*m_r0;
		double r4=r2*r2;

		for(int i=0;i<iter;i++)
		{
			double r_2 = undist[0]*undist[0] + undist[1]*undist[1];
			double r_4 = r_2 * r_2;
			dr  = m_a1*(r_2 - r2) + m_a2*(r_4 - r4);
			undist[0] = dist[0]/(1.0+dr);
			undist[1] = dist[1]/(1.0+dr);
		}
	}
	return undist;
	//End old Version from Roman Calow
	///////////////////////////////////////
	*/
	
}

VectorT<double,3> CalibrationModE::pix2world(VectorT<double,2> &pix, double d/*=1000*/)
{
	//Verzeichnungsbehaftete vorzeichenlose Bildkoordinaten -> verzeichnungskorrigierte vorzeichenbehaftete Sensorkoordnaten
	VectorT<double,2> undist=pix2undistSensor(pix);
	//dann Transformation in das Kamerakoordinatensystem (Definition der Blickrichtung ist die negative z-Achse)
	//und zwar auf die Bildebene mit dem Abstand d vor dem Projektionszentrum (Der Ursprung des Kamerakoordinatensystems)
	VectorT<double,3> cam;
	cam[0]=-d*undist[0] / m_c;
	cam[1] = -d*undist[1] / m_c;
	cam[2] = -d;
	//Transformation des Punktes auf der Bildebene des Kamerakoordinatensystems in das Weltkoordinatensystem
	VectorT<double,3> world;
	world=m_mtxRi.getTranspose()*cam+m_cop;
	return world;
}

/*
//Berechnet f�r das jeweilige CalibrationModE die Epipolarlinie aus einem Pixel eines anderen CalibrationModE "calMod"
void CalibrationModE::getEpiplorarLine(VectorT<double, 2> &pix, CalibrationModE &calMod, double zMin, double zMax, VectorT<double, 2> *a, VectorT<double, 2> *b)
{
	//*a=cameraModel(calMod.get_cop());
	//*b=(cameraModel(calMod.pix2world(pix))-*a).getNormalized();


	//Ray3T<double> dirRay(calMod.get_cop(),calMod.get_cop()-calMod.pix2world(pix,1000));
	Ray3T<double> dirRay(calMod.get_cop(), calMod.pix2world(pix, 1000) - calMod.get_cop());
	
	Plane3T<double> planeXY(VectorT<double,3>(0.0,0.0,0.0),VectorT<double,3>(0.0,0.0,1.0));
	VectorT<double,3> m;//Schnittpunkt des Strahls mit der Ebene
	//double r=5.0;//5.0 Halbe Tiefe des Sichtvolumens
	
	
	double t;
	math::intersectRayPlane<double>(dirRay,planeXY,&t,&m);
	VectorT<double,3> p1=m+dirRay.m_direction*-zMin;
	VectorT<double,3> p2=m+dirRay.m_direction*-zMax;

	*a=model(p1);
	*b=model(p2);

	//cout << p1 <<  m << p2 << endl;
	//cout << *a;
	//cout << *b;
}
*/

VectorT<double,3> CalibrationModE::calc3D(CalibrationModE& calModA, CalibrationModE& calModB, VectorT<double,2>& v2A, VectorT<double,2>& v2B)
{
	VectorT<double,3> weltPA;
	VectorT<double,3> weltPB;
	
	weltPA=calModA.pix2world(v2A);//weltPA ist ein Punkt auf dem Sehstrahl (1m vor der Kamera)
	weltPB=calModB.pix2world(v2B);


	/////////////////////////////////////////////////
	//Vector entlang des Sehstrahles f�r
	//Kamera A			und			Kamera B
	double x11 = calModA.m_cop[0] - weltPA[0];	double x12 = -(calModB.m_cop[0] - weltPB[0]);    double y1 = weltPB[0] - weltPA[0];
	double x21 = calModA.m_cop[1] - weltPA[1];	double x22 = -(calModB.m_cop[1] - weltPB[1]);    double y2 = weltPB[1] - weltPA[1];
	double x31 = calModA.m_cop[2] - weltPA[2];	double x32 = -(calModB.m_cop[2] - weltPB[2]);    double y3 = weltPB[2] - weltPA[2];
	
	// Ausgleichsrechnung (2 Raumgeraden m�ssen sich nicht schneiden.)
	double N11 = x11*x11 + x21*x21 + x31*x31;
	double N12 = x11*x12 + x21*x22 + x31*x32;
	double N21 = x12*x11 + x22*x21 + x32*x31;
	double N22 = x12*x12 + x22*x22 + x32*x32;

	double M1 = x11*y1 + x21*y2 + x31*y3;
	double M2 = x12*y1 + x22*y2 + x32*y3;

	// L�sen des Gleichungssystems
	double D1 = M1*N22 - M2*N12;
	double D2 = N11*M2 - N21*M1;
	double D  = N11*N22 - N21*N12;
	
	double t1 = D1/D;
	double t2 = D2/D;
	

	//dst->x =  (weltPA.x + t1*(kamA->x - weltPA.x) + weltPB.x + t2*(kamB->x - weltPB.x) ) / 2.0;
	//dst->y =  (weltPA.y + t1*(kamA->y - weltPA.y) + weltPB.y + t2*(kamB->y - weltPB.y) ) / 2.0;
	//dst->z =  (weltPA.z + t1*(kamA->z - weltPA.z) + weltPB.z + t2*(kamB->z - weltPB.z) ) / 2.0;

	return ( weltPA+(calModA.m_cop-weltPA)*t1 + weltPB+(calModB.m_cop-weltPB)*t2 )/2.0;
}

/*
VectorT<double, 3> CalibrationModE::calc3D_LS(CalibrationModE& calModA, CalibrationModE& calModB, VectorT<double, 2>& v2A, VectorT<double, 2>& v2B)
{
	VectorT<double, 3> weltPA;
	VectorT<double, 3> weltPB;

	weltPA = calModA.pix2world(v2A);//weltPA ist ein Punkt auf dem Sehstrahl (1m vor der Kamera)
	weltPB = calModB.pix2world(v2B);

	
	
	//Richtungsvektoren vom Projektionszentrum zum Punkt auf Sehstrahl
	VectorT<double, 3> dA = weltPA - calModA.get_cop();
	VectorT<double, 3> dB = weltPB - calModB.get_cop();

	
	//Aufstellen des Gleichungssystems
	int n = 6;//Anzahl der Beobachtungen
	int u = 5;//Anzahl der Unbekannten
	

	DynVectorT<double> L(n, weltPA[0],//Beobachtungsvektor
						weltPA[1],
						weltPA[2],
						weltPB[0],
						weltPB[1],
						weltPB[2]);
	
	DynMatrixT<double> mtxA(n, u);//Koeffizientenmatrix
	mtxA(0, 0) = 1.0; mtxA(0, 1) = 0.0; mtxA(0, 2) = 0.0;	mtxA(0, 3) = dA[0]; mtxA(0, 4) = 0.0;
	mtxA(1, 0) = 0.0; mtxA(1, 1) = 1.0; mtxA(1, 2) = 0.0;	mtxA(1, 3) = dA[1]; mtxA(1, 4) = 0.0;
	mtxA(2, 0) = 0.0; mtxA(2, 1) = 0.0; mtxA(2, 2) = 1.0;	mtxA(2, 3) = dA[2]; mtxA(2, 4) = 0.0;
	
	mtxA(3, 0) = 1.0; mtxA(3, 1) = 0.0; mtxA(3, 2) = 0.0;	mtxA(3, 3) = 0.0; mtxA(3, 4) = dB[0];
	mtxA(4, 0) = 0.0; mtxA(4, 1) = 1.0; mtxA(4, 2) = 0.0;	mtxA(4, 3) = 0.0; mtxA(4, 4) = dB[1];
	mtxA(5, 0) = 0.0; mtxA(5, 1) = 0.0; mtxA(5, 2) = 1.0;	mtxA(5, 3) = 0.0; mtxA(5, 4) = dB[2];

	DynVectorT<double> p(u);//Unbekanntenvektor

	int r = math::leastSquares(mtxA, L, p);
	return VectorT<double, 3>(&p[0]);
}


//Beliebig viele Kameras
VectorT<double, 3> CalibrationModE::calc3D_LS(vector<CalibrationModE*>& rCalModContainer, vector<VectorT<double, 2>*>& rCorrespondences)
{
	size_t nCameras = rCalModContainer.size();
	vector<VectorT<double, 3>> vPointOnRay;
	//vPointOnRay.resize(nCameras);
	vector<VectorT<double, 3>> vDirections;
	//vDirections.resize(nCameras);
	//Berechne f�r jede Korrespondenz einen Punkt auf dem jeweiligen Sehstrahl

	size_t nDirections = 0;
	for (size_t c = 0; c < nCameras; c++)
	{
		if (rCorrespondences[c] != NULL)
		{
			VectorT<double, 3> p = rCalModContainer[c]->pix2world(*rCorrespondences[c]);
			vPointOnRay.push_back(p);//vPointOnRay[c] ist ein Punkt auf dem Sehstrahl (1m vor der Kamera c)
			vDirections.push_back(p - rCalModContainer[c]->get_cop());
			nDirections++;
		}
	}

	size_t n = 3 * nDirections;
	size_t u = 3 + nDirections;
	DynVectorT<double> L(n);//Beobachtungsvektor
	DynMatrixT<double> mtxA(n, u);//Matrix A
	//mtxA.fill(0.0);
	size_t row = 0;
	for (size_t nD = 0; nD < nDirections; nD++)
	{
		//Jedes nd liefert 3 Zeilen, eine f�r jede Koordinate
		for (size_t coordR = 0; coordR < 3; coordR++)
		{
			L[row] = vPointOnRay[nD][coordR];
			//Die ersten drei Spalten der Zeile
			for (size_t coordC1 = 0; coordC1 < 3; coordC1++)
			{
				if (coordR == coordC1)
					mtxA(row, coordC1) = 1.0;
				//else
				//	mtxA(row, coordC1) = 0.0;
			}
			//die restlichen Spalten der Zeile
			for (size_t coordC2 = 3; coordC2 < u; coordC2++)
			{
				if (nD == coordC2 - 3)
					mtxA(row, coordC2) = vDirections[nD][coordR];
				//else
				//{
				//	mtxA(row, coordC2) = 0.0;
				//}
			}
			row++;
		}
		
	}
	//cout << "mtxA" << mtxA << endl;
	DynVectorT<double> p(u);//Unbekanntenvektor
	int r = math::leastSquares(mtxA, L, p);
	return VectorT<double, 3>(&p[0]);
}
*/

void CalibrationModE::calcViewingFrustumDirectionsInWorld(VectorT<double,3>* upperLeft, VectorT<double,3>* upperRight, VectorT<double,3>* lowerRight, VectorT<double,3>* lowerLeft, double width, double height)
{
	double lenProjectionCenterToImagePlane=abs(m_c);												//Der Bildursprung ist oben links definiert
	*upperLeft=pix2world(VectorT<double,2>(0.0,0.0),lenProjectionCenterToImagePlane)-m_cop;			//Richtungsvektor Projektionszentrum -> obere linke Ecke der Bildebene
	*upperRight=pix2world(VectorT<double,2>(width,0.0),lenProjectionCenterToImagePlane)-m_cop;		//Richtungsvektor Projektionszentrum -> obere rechte Ecke der Bildebene
	*lowerRight=pix2world(VectorT<double,2>(width,height),lenProjectionCenterToImagePlane)-m_cop;	//Richtungsvektor Projektionszentrum -> untere rechte Ecke der Bildebene
	*lowerLeft=pix2world(VectorT<double,2>(0,height),lenProjectionCenterToImagePlane)-m_cop;		//Richtungsvektor Projektionszentrum -> untere linke Ecke der Bildebene
}

VectorT<double,3> CalibrationModE::viewingDirThroughPrincipalPoint()
{
	return pix2world(m_pp,-m_c)-m_cop;
}


VectorT<double,3> CalibrationModE::world2camera(VectorT<double,3> &world)
{
	return m_mtxRi*(world-m_cop);
}

VectorT<double,2> CalibrationModE::camera2undistSensor(VectorT<double,3> &cameraCoord)
{
	return VectorT<double,2>(cameraCoord[0]*m_c/cameraCoord[2],cameraCoord[1]*m_c/cameraCoord[2]);
}

VectorT<double,2> CalibrationModE::distortRadial(VectorT<double,2> &undistSensor)
{
	double rs=undistSensor.length(); //distance (xs,ys) to principal point
	double rs_2=rs*rs;
	double rs_4=rs_2*rs_2;

	//double dr  = m_a1*(r_2 - m_r0_2) + m_a2*(r_4 - m_r0_4);
	double drrad=m_a1*(rs_2-m_r0_2)+m_a2*(rs_4-m_r0_4);

	//Verzeichnete Sensorkoordinaten
	return undistSensor+(undistSensor*drrad);
}


int CalibrationModE::setupFromKAM(const TCHAR *fileName) //reads the parameters from .Kam file and feeds into the variables of this class
{
	tifstream infile(fileName);
	if (infile.is_open())
	{
		m_strName=fileName;
		TCHAR section[512]={0};
		TCHAR buffer[512]={0};
		TCHAR* rest;
		TCHAR trash[512]={0};
		float t=0;
		
		streamsize externpos=0;
		streamsize internpos=0;
		streamsize eos=0;	//end of section --> a section ends with an empty line
		while(!infile.eof())
		{
			infile.getline(trash,sizeof(trash),_T('['));
			infile.getline(section,sizeof(section),_T(']'));
			TCHAR* s=NULL;
			if (_tcsicmp(section,_T("EXTERN"))==0)
			{
				infile.getline(buffer,sizeof(buffer),_T('\n'));
				do 
				{
					infile.getline(buffer,sizeof(buffer));
					s=_tcstok_s(buffer,_T(" \n="),&rest);
					if (s!=NULL)
					{
						//Position
						if (_tcsicmp(s,_T("X"))==0)
							m_cop[0]=_tstof(rest);
						else if (_tcsicmp(s,_T("Y"))==0)
							m_cop[1]=_tstof(rest);
						else if (_tcsicmp(s,_T("Z"))==0)
							m_cop[2]=_tstof(rest);
						//Orientierung
						else if (_tcsicmp(s,_T("Om"))==0)
							m_omega=_tstof(rest);
						else if (_tcsicmp(s,_T("Phi"))==0)
							m_phi=_tstof(rest);
						else if (_tcsicmp(s,_T("Kap"))==0)
							m_kappa=_tstof(rest);
					}
				}
				while(s!=0);
			}
			if (_tcsicmp(section,_T("INTERN"))==0)
			{
				infile.getline(buffer,sizeof(buffer),_T('\n'));
				do 
				{
					infile.getline(buffer,sizeof(buffer));
					s=_tcstok_s(buffer,_T("="),&rest);
					if (s!=NULL)
					{
						if (_tcsicmp(s,_T("Hx"))==0)
							m_pp[0]=_tstof(rest);
						else if (_tcsicmp(s,_T("Hy"))==0)
							m_pp[1]=_tstof(rest);
						else if (_tcsicmp(s,_T("C"))==0)
							m_c=_tstof(rest);
						else if (_tcsicmp(s,_T("Sy"))==0)
							m_sy=_tstof(rest);
						else if (_tcsicmp(s,_T("A1"))==0)
							m_a1=_tstof(rest);
						else if (_tcsicmp(s,_T("A2"))==0)
							m_a2=_tstof(rest);
						else if (_tcsicmp(s, _T("R0")) == 0)
						{
							set_r0(_tstof(rest));//set m_r0 as well as m_r0_2 and m_r0_4
						}
					
					}
				}
				while(s!=0);
			}
			int i=0;
		}
		infile.close();
		makeInvRotationMatrix();
	}
	else
	{
		tcout << __FILE__ << _T("(") << __LINE__ << _T(")\n") << _T("Cannot load ") << fileName << std::endl;
		return 0;
	}
	return 1;
}

void CalibrationModE::writeKAMFile(const TCHAR* fileName)
{
	tofstream outfile(fileName);
	outfile.precision(18);
	if (outfile.is_open())
	{
		outfile << fixed << _T("[EXTERN]") << endl
			<<_T("X=") << m_cop[0]<<_T("\t;\tX-Projektionszentrum [mm]")<<endl
			<<_T("Y=")<<m_cop[1]<<_T("\t;\tY-Projektionszentrum [mm]")<<endl
			<<_T("Z=")<<m_cop[2]<<_T("\t;\tZ-Projektionszentrum [mm]")<<endl
			<<_T("Om=")<<m_omega<<_T("\t;\tRotation x [rad]")<<endl
			<<_T("Phi=")<<m_phi<<_T("\t;\tRotation y [rad]")<<endl
			<<_T("Kap=")<<m_kappa<<_T("\t;\tRotation z [rad]")<<endl
			<<endl
			<<_T("[INTERN]")<<endl
			<<_T("Hx=")<<m_pp[0]<<_T("\t;\tX-Hauptpunkt [Pixel]")<<endl
			<<_T("Hy=")<<m_pp[1]<<_T("\t;\tY-Hauptpunkt [Pixel]")<<endl
			<<_T("C=")<<m_c<<_T("\t;\tKamerakonstante [Pixel]")<<endl
			<<_T("Sy=")<<m_sy<<_T("\t;\tY-Pixelscalierung negativ, wenn Bildursprung linksoben")<<endl
			<<_T("R0=")<<m_r0<<_T("\t;\tzweiter Nulldurchgang [Pixel]")<<endl
			<<scientific
			<<_T("A1=")<<m_a1<<_T("\t;\tVerzeichnung1")<<endl
			<<_T("A2=")<<m_a2<<_T("\t;\tVerzeichnung2")<<endl;
		outfile.close();
	}
}


VectorT<double,3> CalibrationModE::get_cop()
{
	return m_cop;
}

double CalibrationModE::get_omega()
{
	return m_omega;
}

double CalibrationModE::get_phi()
{
	return m_phi;
}

double CalibrationModE::get_kappa()
{
	return m_kappa;
}

MatrixT<double,3,3> CalibrationModE::get_mtxR()
{
	return m_mtxRi;
}

double CalibrationModE::get_c()
{
	return m_c;
}

double CalibrationModE::get_sy()
{
	return m_sy;
}

VectorT<double,2> CalibrationModE::get_pp()
{
	return m_pp;
}

double CalibrationModE::get_a1()
{
	return m_a1;
}

double CalibrationModE::get_a2()
{
	return m_a2;
}
	
double CalibrationModE::get_r0()
{
	return m_r0;
}

void CalibrationModE::set_r0(double r0)
{
	m_r0 = r0;
	m_r0_2 = m_r0*m_r0;
	m_r0_4 = m_r0_2*m_r0_2;
}

void CalibrationModE::printParameters()
{
	_tprintf(_T("Camera %s\ncop: %.10f %.10f %.10f\nAngles [rad]: %.10f %.10f %.10f\nc: %.10f\nsy: %.10f\npp: %.10f %.10f\nr0: %.10f\na1: %e\na2: %e\n\n"),
		m_strName.c_str(),m_cop[0],m_cop[1],m_cop[2],m_omega,m_phi,m_kappa,m_c,m_sy,m_pp[0],m_pp[1],m_r0,m_a1,m_a2);
	/*
	tcout << m_strName << endl
		<< _T("Centre of Projection: ") << m_cop << endl
		<< _T("Euler angles: ") << m_omega << _T(", ") << m_phi << _T(", ") << m_kappa << endl
		<< _T("Camera constant: ") << m_c << endl
		<< _T("vertical scaling: ") << m_sy << endl
		<< _T("Principal Point: ") << m_pp << endl
		<< _T("R0: ") << m_r0 << endl
		<< _T("A1: ") << m_a1 << endl
		<< _T("A2: ") << m_a2 << endl;
		*/
		
}