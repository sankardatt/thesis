//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
#pragma once
#include "CalibrationMod.h"
#include <vector>

//#include "CalibrationModE.h"

//includes for equation solver
#include <Eigen/LU>
#include <Eigen/SparseQR>
#include <Eigen/Core>
#include <iostream>

using namespace Eigen;

//#include "MathHelperFunc.h"
//#include "Intersection.h"
/*
#include <UnicodeHelper.h>
#include <MatrixT.h>
#include <PassPoint.h>

#include <list>
#include <fstream>
using namespace std;
*/

class CalibrationModE : public CalibrationMod
{
public:
	/////////////////////////////////////////////////////////////
	// Outer orientation	- Camera location and orientation in the world coordinate system
	//	Luhmann 
	VectorT<double,3> m_cop;	//!< O (X0, Y0, Z0)	lense location -> center of projection 
	
	double m_omega;	//!< rotation around x in radians (world)(pitch)
	double m_phi;	//!< rotation around y in radians (world)(heading)
	double m_kappa;	//!< rotation around z in radians (world)(roll)
	MatrixT<double,3,3> m_mtxRi; //!< current inverse of rotation matrix (Used in camera model to project 3d world coordinates to image plane)
	
	////////////////////////////////////////////////////////////
	// Inner orientation - Location of projection center in image coordinate system
	//						 and differences from mathematical model of the central projection
	double m_c;		//!< camera constant
	double m_sy;		//!< pixel scaling

	VectorT<double,2> m_pp; //!< drop a perpendicular from lense to image sensor -> principal point

	//! radial symmetric distortion parameters
	double m_a1;
	double m_a2;
	double m_r0;
	
	////////////////////////////////////////////////////////////

	double m_dh;//!< helper to compute difference quotient
	double m_dh2;//!< helper to compute difference quotient

	double transformationMat[4][4];

private:
	double m_r0_2;
	double m_r0_4;

public:
	CalibrationModE(void);
	~CalibrationModE(void);

	VectorT<double,3> get_cop();
	double get_omega();
	double get_phi();
	double get_kappa();
	MatrixT<double,3,3> get_mtxR();
	double get_c();
	double get_sy();
	VectorT<double,2> get_pp();
	double get_a1();
	double get_a2();
	double get_r0();
	void set_r0(double r0);

	//!Computes the rotationmatrix from omega, phi and kappa
	void makeInvRotationMatrix();


	//!Difference quotient calculations
	VectorT<double, 2> dq_c(ControlPointT<double, 3>& pass3D);
	
	VectorT<double, 2> dq_ppx(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_ppy(ControlPointT<double, 3>& pass3D);
	
	VectorT<double, 2> dq_copx(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_copy(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_copz(ControlPointT<double, 3>& pass3D);

	VectorT<double, 2> dq_omega(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_phi(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_kappa(ControlPointT<double, 3>& pass3D);

	VectorT<double, 2> dq_sy(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_a1(ControlPointT<double, 3>& pass3D);
	VectorT<double, 2> dq_a2(ControlPointT<double, 3>& pass3D);


	//!Direct linear transformation: pre-calibration to step to estimate
	//!camera's position, orientation, focal length and principal point.
	int dlt();

    //Evaluates the reprojection to testify the answer
    void reprojection(Vector3d, double [][3], double*);
    //Finds the external parameters, given the internals
    int externs(double *intrinsics);

	//!Starts the calibration
	void startCalib();

	//!From the world frame to camera frame
	VectorT<double,3> world2camera(VectorT<double,3> &world);

	//!From the camera frame to signed undistorted sensor coords
	VectorT<double,2> camera2undistSensor(VectorT<double,3> &cameraCoord);

	//!Applying radial distortion --> signed distorted sensor coordinates
	VectorT<double,2> distortRadial(VectorT<double,2> &undistSensor);
	

	//! Convert unsigned pixel coordinates to undistorted signed sensor coordinates when
	//! subtracting the principal point m_pp and applying radial distortion correction
	VectorT<double,2> pix2undistSensor(VectorT<double,2> &pix);

	//! computes parameters for epipolar line pe=a+tb;
	//!Berechne die Epipolarlinie eines Pixels in A im Bild C
	//! m_calModC.getEpiplorarLine(pixA,m_calModA,&a,&b);
	void getEpiplorarLine(VectorT<double, 2> &pix, CalibrationModE& calMod, double zMin, double zMax, VectorT<double, 2>* a, VectorT<double, 2>* b);

	//!Calculates 3D coordinates from two calibration
	static VectorT<double,3> calc3D(CalibrationModE& calModA, CalibrationModE& calModB, VectorT<double,2>& v2A, VectorT<double,2>& v2B);

	//!Calculates 3D coordinates from two calibration as least squares problem
	static VectorT<double, 3> calc3D_LS(CalibrationModE& calModA, CalibrationModE& calModB, VectorT<double, 2>& v2A, VectorT<double, 2>& v2B);

	//!Calculates 3D coordinates from two calibration as least squares problem
	static VectorT<double, 3> calc3D_LS(vector<CalibrationModE*>& rCalModContainer, vector<VectorT<double, 2>*>& rCorrespondences);

	//! Computes directions from Prokection center to upper-left, upper-right, lower-left, lower-right image plane corners in world
	void calcViewingFrustumDirectionsInWorld(VectorT<double,3>* upperLeft, VectorT<double,3>* upperRight, VectorT<double,3>* lowerRight, VectorT<double,3>* lowerLeft, double width, double height);  

	VectorT<double,3> viewingDirThroughPrincipalPoint();

	void printParameters();

	//Overrides

	//!Sets default values for parameters
	void setParametersToDefault();
	//!Estimate Parameters by calling dlt
	void estimateParameters();
	//!Loading parameters from kam-file
	int setupFromKAM(const TCHAR *fileName);
	//!Save prameters to kam-file
	void writeKAMFile(const TCHAR *fileName);

	//!Transformation from world frame to image frame
	//!by collinearity equations
	VectorT<double, 2> model(VectorT<double, 3>& p);

	//! Creates a point on viewing ray in world coordinates
	VectorT<double, 3> pix2world(VectorT<double, 2> &pix, double d = 1000);
};
