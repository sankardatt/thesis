//(C)opyright Christian Bendicks, Otto-von-Guericke University Magdeburg
#pragma once
#include <tchar.h>
#include <iostream>
using namespace std;
#include "VectorT.h"


//!@brief ControlPointT<T,_m> template class
//!
//! This class represents ... .



template<class T, size_t _m=1>
class ControlPointT
{
private:
	int m_id;

	VectorT<T, _m> m_coord;
	VectorT<T, _m> m_std;
	

public:
	ControlPointT() :m_id(-1)
	{

	}

	ControlPointT(const VectorT<T, _m>& coord, int id=-1)
	{
		m_id = id;
		m_coord = coord;
	}

	//! Copy constructor
	//! is called when ControlPointT<type> cp2=cp1;
	ControlPointT(const ControlPointT& cp)
	{
		m_id = cp.m_id;
		m_coord = cp.m_coord;
		m_std = cp.m_std;
	}

	
	VectorT<T, _m>& coord()
	{
		return m_coord;
	}

	const VectorT<T, _m>& coord() const
	{
		return m_coord;
	}

	VectorT<T, _m>& std()
	{
		return m_std;
	}

	const VectorT<T, _m>& std() const
	{
		return m_std;
	}
	

	int& id()
	{
		return m_id;
	}
	
	const int& id() const
	{
		return m_id;
	}

	//!@brief Equals operator.
	bool operator==(const ControlPointT<T, _m> &_Right) const { return m_id == _Right.m_id; }
	bool operator!=(const ControlPointT<T, _m> &_Right) const { return m_id != _Right.m_id; }
	bool operator>(const ControlPointT<T, _m> &_Right) const { return m_id>_Right.m_id; }
	bool operator<(const ControlPointT<T, _m> &_Right) const { return m_id<_Right.m_id; }
	bool operator<=(const ControlPointT<T, _m> &_Right) const { return m_id <= _Right.m_id; }
	bool operator>=(const ControlPointT<T, _m> &_Right) const { return m_id >= _Right.m_id; }

	ControlPointT& operator=(const ControlPointT &cp) { m_id = cp.m_id; m_coord = cp.m_coord; m_std = cp.m_std; return *this; }

	
	//! Standard output
	template<class T>
	friend std::ostream& operator<<(std::ostream &s, ControlPointT<T, _m> &cp);

	//! Standard output
	template<class T>
	friend std::wostream& operator<<(std::wostream &s, ControlPointT<T, _m> &cp);
	
	/*
	//! Standard intput
	template<class T, size_t _m>
	friend std::istream<TCHAR, char_traits<TCHAR> >& operator>>(std::basic_istream<TCHAR, char_traits<TCHAR> > &s, ControlPointT<T, _m> &cp);
	*/
	template<class T>
	friend std::istream& operator<<(std::istream &s, ControlPointT<T, _m> &cp);

	template<class T>
	friend std::wistream& operator<<(std::wistream &s, ControlPointT<T, _m> &cp);
};


template<class T, size_t _m>
std::ostream& operator<< (std::ostream& s,  ControlPointT<T, _m> &cp)
{
	s << cp.id();

	for (int i = 0; i < _m; i++)
	{
		s << "\t" << cp.coord()[i];
	}
	for (int i = 0; i < _m; i++)
	{
		s << "\t" << cp.std()[i];
	}
	return s;
}

template<class T, size_t _m>
std::wostream& operator<< (std::wostream& s, ControlPointT<T, _m> &cp)
{
	s << cp.id();

	for (int i = 0; i < _m; i++)
	{
		s << L"\t" << cp.coord()[i];
	}
	for (int i = 0; i < _m; i++)
	{
		s << L"\t" << cp.std()[i];
	}
	return s;
}


template<class T, size_t _m>
std::wistream& operator<<(std::wistream &s, ControlPointT<T, _m> &cp)
{
	s >> cp.id();

	for (int i = 0; i < _m; i++)
	{
		s >> cp.coord()[i];
	}
	for (int i = 0; i < _m; i++)
	{
		s >> cp.std()[i];
	}
	return s;
}

template<class T, size_t _m>
std::istream& operator<<(std::istream &s, ControlPointT<T, _m> &cp)
{
	s >> cp.id();

	for (int i = 0; i < _m; i++)
	{
		s >> cp.coord()[i];
	}
	for (int i = 0; i < _m; i++)
	{
		s >> cp.std()[i];
	}
	return s;
}

