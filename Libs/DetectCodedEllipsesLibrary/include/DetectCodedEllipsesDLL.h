// DetectCodedEllipsesDll.h

#ifdef DETECTCODEDELLIPSESDLL_EXPORTS
#define DETECTCODEDELLIPSESDLL_API __declspec(dllexport) 
#else
#define DETECTCODEDELLIPSESDLL_API __declspec(dllimport) 
#endif

namespace DCE
{

	//! Struct containing marker information
	struct CodedMarker{
		int _id;//!< Identifier
		double _x;//!< x coordinate for center
		double _y;//!< y coordinate for center
		double _stdX;//!< x component of standard deviation. A small value indicates a well shaped ellipse. 
		double _stdY;//!< y component of standard deviation. A small value indicates a well shaped ellipse.
	};

	//! Class to detect coded ellipsoid markers.
	class CodedEllipsesDetector
	{
	public:
		//! Initialize mark detector.
		//! \param nRays number of rays intersecting ellipsoid contour from a point inside - for ellipse fitting
		//! \param nRayLen Length of rays in pixel. Set this length to the supposed maximum ellipsoid diameter x2.
		//! \param bBlackOnWhite Flag to state that marks are black and background is white (1). If zero marks are stated to be white on black background.
		//! \param bMeasureTwice Indicates to measureme twice (1) or single (0).
		//! \param edgeThreshold Threshold for grayscale level after convolution [default is 40]. Small value means detect weak edges also, higher values means to detect sharp edges.
		//! \param nCodeRingSamples Number of samples to identify code ring.
		//! \param detectionAlg Set edge detection algorithm. 0 - classical edge detection based on root. 1 - Using iso contours (with thresh=0.5(gwmin+gwmax).
		//! \param codeListName Path to code list file.
		static DETECTCODEDELLIPSESDLL_API void setOptions(int nRays, int nRayLen, int bBlackOnWhite, int bMeasureTwice, double edgeThreshold, int nCoderingSamples, int detectionAlg, const char* codeListName);
		
		//! Setting image for mark detection.
		//! \param pData Address to image data.
		//! \param width Image width.
		//! \param height Image height.
		static DETECTCODEDELLIPSESDLL_API void setBitmap(unsigned char* pData, int width, int height);

		//! Detect markers in grayscale image.
		//! \param pFoundMarkers Array address to store detected CodeMarkers.
		//! \param foundMax Maximum of expected markers in image. Size of CodeMarker Array.
		//! \return Number of detected markers.
		static DETECTCODEDELLIPSESDLL_API int detect(CodedMarker* pFoundMarkers, int foundMax);
		
		//! Identify a marker on specified image position.
		//! \param x x coordinate of position.
		//! \param y y coordinate of position.
		//! \param marker Contains marker info of a detected marker.
		//! \return Identifier or -1 if there is no marker.
		static DETECTCODEDELLIPSESDLL_API int detectAt(int x, int y, CodedMarker* marker);
	};
}