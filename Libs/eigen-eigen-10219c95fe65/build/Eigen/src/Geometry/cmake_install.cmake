# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/AlignedBox.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/AngleAxis.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/EulerAngles.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Homogeneous.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Hyperplane.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/OrthoMethods.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/ParametrizedLine.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Quaternion.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Rotation2D.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/RotationBase.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Scaling.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Transform.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Translation.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry/Umeyama.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/Geometry" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/AlignedBox.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/AngleAxis.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/EulerAngles.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Homogeneous.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Hyperplane.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/OrthoMethods.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/ParametrizedLine.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Quaternion.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Rotation2D.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/RotationBase.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Scaling.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Transform.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Translation.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/Geometry/Umeyama.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/build/Eigen/src/Geometry/arch/cmake_install.cmake")

endif()

