# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLUImpl.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_column_bmod.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_column_dfs.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_copy_to_ucol.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_gemm_kernel.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_heap_relax_snode.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_kernel_bmod.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_Memory.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_panel_bmod.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_panel_dfs.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_pivotL.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_pruneL.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_relax_snode.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_Structs.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_SupernodalMatrix.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU/SparseLU_Utils.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseLU" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLUImpl.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_column_bmod.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_column_dfs.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_copy_to_ucol.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_gemm_kernel.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_heap_relax_snode.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_kernel_bmod.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_Memory.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_panel_bmod.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_panel_dfs.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_pivotL.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_pruneL.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_relax_snode.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_Structs.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_SupernodalMatrix.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseLU/SparseLU_Utils.h"
    )
endif()

