# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR/ColPivHouseholderQR.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR/ColPivHouseholderQR_MKL.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR/FullPivHouseholderQR.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR/HouseholderQR.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR/HouseholderQR_MKL.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/QR" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR/ColPivHouseholderQR.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR/ColPivHouseholderQR_MKL.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR/FullPivHouseholderQR.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR/HouseholderQR.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/QR/HouseholderQR_MKL.h"
    )
endif()

