# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/AmbiVector.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/CompressedStorage.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/MappedSparseMatrix.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseBlock.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseColEtree.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseCwiseBinaryOp.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseCwiseUnaryOp.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseDenseProduct.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseDiagonalProduct.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseDot.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseFuzzy.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseMatrix.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseMatrixBase.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparsePermutation.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseProduct.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseRedux.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseSelfAdjointView.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseSparseProductWithPruning.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseTranspose.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseTriangularView.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseUtil.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseVector.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/SparseView.h;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore/TriangularSolver.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/src/SparseCore" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/AmbiVector.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/CompressedStorage.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/MappedSparseMatrix.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseBlock.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseColEtree.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseCwiseBinaryOp.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseCwiseUnaryOp.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseDenseProduct.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseDiagonalProduct.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseDot.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseFuzzy.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseMatrix.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseMatrixBase.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparsePermutation.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseProduct.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseRedux.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseSelfAdjointView.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseSparseProductWithPruning.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseTranspose.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseTriangularView.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseUtil.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseVector.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/SparseView.h"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/src/SparseCore/TriangularSolver.h"
    )
endif()

