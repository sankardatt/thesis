# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Array;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Cholesky;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/CholmodSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Core;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Dense;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Eigen;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Eigen2Support;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Eigenvalues;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Geometry;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Householder;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/IterativeLinearSolvers;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Jacobi;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/LeastSquares;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/LU;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/MetisSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/OrderingMethods;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/PardisoSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/PaStiXSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/QR;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/QtAlignedMalloc;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/Sparse;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SparseCholesky;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SparseCore;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SparseLU;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SparseQR;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SPQRSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/StdDeque;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/StdList;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/StdVector;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SuperLUSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/SVD;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen/UmfPackSupport")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/Eigen" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Array"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Cholesky"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/CholmodSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Core"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Dense"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Eigen"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Eigen2Support"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Eigenvalues"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Geometry"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Householder"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/IterativeLinearSolvers"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Jacobi"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/LeastSquares"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/LU"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/MetisSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/OrderingMethods"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/PardisoSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/PaStiXSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/QR"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/QtAlignedMalloc"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/Sparse"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SparseCholesky"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SparseCore"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SparseLU"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SparseQR"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SPQRSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/StdDeque"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/StdList"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/StdVector"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SuperLUSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/SVD"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/Eigen/UmfPackSupport"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/build/Eigen/src/cmake_install.cmake")

endif()

