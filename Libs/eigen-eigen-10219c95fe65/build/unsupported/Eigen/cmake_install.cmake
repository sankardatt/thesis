# Install script for directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/AdolcForward;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/BVH;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/IterativeSolvers;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/MatrixFunctions;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/MoreVectorization;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/AutoDiff;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/AlignedVector3;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/Polynomials;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/FFT;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/NonLinearOptimization;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/SparseExtra;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/IterativeSolvers;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/NumericalDiff;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/Skyline;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/MPRealSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/OpenGLSupport;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/KroneckerProduct;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/Splines;E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen/LevenbergMarquardt")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/install/include/eigen3/unsupported/Eigen" TYPE FILE FILES
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/AdolcForward"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/BVH"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/IterativeSolvers"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/MatrixFunctions"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/MoreVectorization"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/AutoDiff"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/AlignedVector3"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/Polynomials"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/FFT"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/NonLinearOptimization"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/SparseExtra"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/IterativeSolvers"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/NumericalDiff"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/Skyline"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/MPRealSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/OpenGLSupport"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/KroneckerProduct"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/Splines"
    "E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/unsupported/Eigen/LevenbergMarquardt"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/build/unsupported/Eigen/src/cmake_install.cmake")

endif()

