# CMake generated Testfile for 
# Source directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65
# Build directory: E:/Master/Libs/3rdParty/eigen-eigen-10219c95fe65/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(Eigen)
subdirs(doc)
subdirs(test)
subdirs(blas)
subdirs(lapack)
subdirs(unsupported)
subdirs(demos)
subdirs(scripts)
